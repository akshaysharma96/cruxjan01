package stackandqueues;

import L17.QueueEmptyException;
import Lecture15.Stack;
import Lecture15.StackEmptyExcemption;

public class Q1 {
	
	Stack<Integer> s1;
	Stack<Integer> s2;
	
	public Q1() {
		s1=new Stack<Integer>();
		s2=new Stack<Integer>();
	}
	
	public void enqueue(int data) throws QueueEmptyException{
		
		s1.push(data);
	}
	
	public int  dequeue() throws QueueEmptyException, StackEmptyExcemption{
		if(s1.isEmpty()){
			throw new QueueEmptyException();
		}
		int temp;
		while(s1.size()!=1){
			temp=s1.pop();
			s2.push(temp);
		}
		
		temp=s1.pop();
		Stack<Integer> temp_stack=s1;
		s1=s2;
		s2=temp_stack;
	
		return temp;
	}
	
	public static void main(String args[]) throws QueueEmptyException, StackEmptyExcemption{
		Q1 q= new Q1();
		for(int i=0;i<7;i++){
			q.enqueue(i);
		}
		
		System.out.println(q.dequeue());
	}
	
	
}
