package stackandqueues;

import Lecture15.Stack;
import Lecture15.StackEmptyExcemption;

public class CheckDuplicate {


	public static boolean matchParenthesis(String str) throws StackEmptyExcemption{
		Stack<Character> st =new Stack<>();
		if(str.length()==1||str.length()==0)
			return true;
		
		for(int i=0;i<str.length();i++){
			char charc=str.charAt(i);
			if(charc=='('||charc=='['||charc=='{')
				st.push(charc);
			
			if(charc=='}'||charc==']'||charc==')'){
				char popped=st.pop();
				if(charc=='}'&&popped!='{')
					return false;
				else if(charc==')'&&popped!='(')
					return false;
				else if(charc==']'&&popped!='[')
					return false;
			}
		}
		
		return true;
	}
	
	public static void main(String[]args){
		String str="{{(a)})}";
		try {
			System.out.println(matchParenthesis(str));
		} catch (StackEmptyExcemption e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
