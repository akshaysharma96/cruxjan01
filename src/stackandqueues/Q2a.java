package stackandqueues;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;
import StackAndQueues.StackEmptyException;

public class Q2a {

	Queue<Integer> q1;
	Queue<Integer> q2;
	
	
	public Q2a() {
		q1=new Queue<>();
		q2=new Queue<>();
	}
	
	public void push(int data) throws QueueEmptyException{
		q1.enqueue(data);
	}
	
	public int  pop() throws StackEmptyException, QueueEmptyException{
		if(q1.isEmpty()){
			throw new StackEmptyException();
		}
		int temp;
		while(q1.size()!=1){
			temp=q1.dequeue();
			q2.enqueue(temp);
		}
		
		temp=q1.dequeue();
		Queue<Integer> temp_q=new Queue<>();
		temp_q=q1;
		q1=q2;
		q2=temp_q;
		
		return temp;
	}
	
	public static void main(String args[]){
		Q2a s=new Q2a();
		for(int i=0;i<8;i++){
			try {
				s.push(i);
			} catch (QueueEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			System.out.println(s.pop());
		} catch (StackEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueueEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
