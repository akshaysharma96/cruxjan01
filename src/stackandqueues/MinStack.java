package stackandqueues;

import StackAndQueues.Stack;
import StackAndQueues.StackEmptyException;

public class MinStack {
	Stack<Integer> original;
	Stack<Integer> min;
	
	public MinStack() {
		original=new Stack<>();
		min=new Stack<>();
	}
	
	public void push(int data) throws StackEmptyException{
		if(original.isEmpty()){
			min.push(data);
		}
		
		else{
			if(data<min.top())
				min.push(data);
		}
		original.push(data);
	}
	
	public int getMin() throws StackEmptyException{
		return min.top();
	}
	
	public int pop() throws StackEmptyException{
		int data=original.pop();
		if(data==min.top())
			min.pop();
		return data;
	}
	
	public static void main(String[]args){
		MinStack min=new MinStack();
		for(int i=0;i<10;i++){
			try {
				min.push(i);
			} catch (StackEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			System.out.println(min.getMin());
		} catch (StackEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
