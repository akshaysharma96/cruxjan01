package stackandqueues;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class Q2b {
	Queue<Integer> q1;
	Queue<Integer> q2;
	
	public Q2b() {
		q1=new Queue<Integer>();
		q2=new Queue<Integer>();
	}
	
	public void push(int data) throws QueueEmptyException{
		int temp;
		while(q1.size()!=0){
			temp=q1.dequeue();
			q2.enqueue(temp);
		}
		
		q1.enqueue(data);
		while(q2.size()!=0){
			temp=q2.dequeue();
			q1.enqueue(temp);
		}
	}
	
	public int pop() throws QueueEmptyException{
		return q1.dequeue();
	}
	
	public static void main(String[]args){
		Q2b s=new Q2b();
		for(int i=0;i<8;i++){
			try {
				s.push(i);
			} catch (QueueEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			System.out.println(s.pop());
		} catch (QueueEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
