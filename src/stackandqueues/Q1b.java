package stackandqueues;

import L17.QueueEmptyException;
import Lecture15.Stack;
import Lecture15.StackEmptyExcemption;

public class Q1b {
	Stack<Integer> s1;
	Stack<Integer> s2;
	
	public Q1b() {
		s1=new Stack<Integer>();
		s2=new Stack<Integer>();
	}
	
	public void enqueue(int data) throws StackEmptyExcemption{
		while(s1.size()!=0){
			int temp=s1.pop();
			s2.push(temp);
		}
		
		s1.push(data);
		while(s2.size()!=0){
			int temp=s2.pop();
			s1.push(temp);
		}
	}
	
	public int dequeue() throws StackEmptyExcemption, QueueEmptyException{
		if(s1.isEmpty()){
			throw new QueueEmptyException();
		}
		return s1.pop();
	}
	
	public static void main(String [] args){
		Q1b q = new Q1b();
		try {
			q.enqueue(5);
			q.enqueue(7);
		} catch (StackEmptyExcemption e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			System.out.println(q.dequeue());
		} catch (StackEmptyExcemption | QueueEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
