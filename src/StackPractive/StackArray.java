package StackPractive;

import Stack.StackFullException;
import StackAndQueues.StackEmptyException;

public class StackArray {
	private int[] stack;
	private int top=-1;
	private int size=0;
	
	
	public StackArray(int length) {
		stack=new int[length];
	}
	
	public boolean isEmpty(){
		return top==-1;
	}
	
	public void push(int data) throws StackFullException{
		if(top==stack.length){
			throw new StackFullException();
		}
		
		top++;
		stack[top]=data;
		size++;
		
	}
	
	public int pop() throws StackEmptyException{
		if(top==-1){
			throw new StackEmptyException();
		}
		int value=stack[top];
		size--;
		top--;
		return value;
	}
	
	public int top(){
		return stack[top];
	}
	
	public int size(){
		return top;
	}
	
	public static void main(String[] args){
		StackArray stack=new StackArray(10);
		for(int i=0;i<7;i++){
			try {
				stack.push(i);
			} catch (StackFullException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(int i=0;i<5;i++){
			try {
				System.out.println(stack.pop());
			} catch (StackEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
//		System.out.println(stack.size());
	}
}
