import java.util.Scanner;


public class CalculateSalary {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		double basic,hra,da,allow,pf,totalSalary;
		char grade;
		System.out.print("Enter your basic salary: ");
		basic=sc.nextDouble();
		System.out.print("Enter your grade: ");
		grade=sc.next().charAt(0);
		if(grade=='A'){
			allow=1700;
		}
		else if(grade=='B'){
			allow=1500;
		}
		else if (grade=='C'){
			allow=1300;
		}
		else{
			System.out.println("Invalid grade");
			return;
		}
		hra=0.2*basic;
		da=0.5*basic;
		pf=0.11*basic;
		totalSalary=basic+hra+da+allow-pf;
		System.out.println("The total salary is: "+totalSalary);
		
	}

}
