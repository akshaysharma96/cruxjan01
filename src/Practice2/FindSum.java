package Practice2;

public class FindSum {

	/**
	 * Find two elements in a array whose sum is equal to given number
	 */

	public static int[] merge(int[] arr1, int[] arr2){
		int[] result=new int[arr1.length+arr2.length];
		int i=0;int j=0;int k=0;
		while(i<arr1.length&&j<arr2.length){
			if(arr1[i]<arr2[j]){
				result[k++]=arr1[i++];
			}
			else{
				result[k++]=arr2[j++];
			}
		}

		while(i<arr1.length){
			result[k++]=arr1[i++];
		}
		while(j<arr2.length){
			result[k++]=arr2[j++];
		}

		return result;
	}


	public static int[] mergeSort(int[] arr, int startIndex, int lastIndex){
		if(startIndex>=lastIndex){
			return arr;
		}
		int[] leftHalf= new int[arr.length/2];
		int[] rightHalf = new int[arr.length-leftHalf.length];
		int i=0;int j=0;
		for(i=0;i<leftHalf.length;i++){
			leftHalf[i]=arr[i];
		}

		for(j=0;i<arr.length;i++,j++){
			rightHalf[j]=arr[i];
		}

		leftHalf=mergeSort(leftHalf,0, leftHalf.length-1);
		rightHalf=mergeSort(rightHalf,0,rightHalf.length-1);

		return merge(leftHalf,rightHalf);
	}
	public static void findPairs(int []sortedArray,int number){
		//Sorted array as input only
		boolean isFound=false;
		int i =0;int j=sortedArray.length-1;
		while(i<j){
			if(sortedArray[i]+sortedArray[j]==number){
				System.out.println("Pairs are: "+"("+sortedArray[i]+","+sortedArray[j]+")");
				isFound=true;
				j--;
			}

			if(sortedArray[i]+sortedArray[j]>number)
				j--;


			if(sortedArray[i]+sortedArray[j]<number)
				i++;
		}

		if(!isFound){
			System.out.println("No pairs exist");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr ={7,8,5,6,4,6};
		arr=mergeSort(arr,0,arr.length-1);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		findPairs(arr,14);
	}	

}
