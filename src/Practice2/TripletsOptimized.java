package Practice2;

public class TripletsOptimized {

	/**
	 * Find the triplets in a array that sum to a particular number
	 */
	public static int[] merge(int[] arr1, int[] arr2){
		int[] result=new int[arr1.length+arr2.length];
		int i=0;int j=0;int k=0;
		while(i<arr1.length&&j<arr2.length){
			if(arr1[i]<arr2[j]){
				result[k++]=arr1[i++];
			}
			else{
				result[k++]=arr2[j++];
			}
		}

		while(i<arr1.length){
			result[k++]=arr1[i++];
		}
		while(j<arr2.length){
			result[k++]=arr2[j++];
		}

		return result;
	}


	public static int[] mergeSort(int[] arr, int startIndex, int lastIndex){
		if(startIndex>=lastIndex){
			return arr;
		}
		int[] leftHalf= new int[arr.length/2];
		int[] rightHalf = new int[arr.length-leftHalf.length];
		int i=0;int j=0;
		for(i=0;i<leftHalf.length;i++){
			leftHalf[i]=arr[i];
		}

		for(j=0;i<arr.length;i++,j++){
			rightHalf[j]=arr[i];
		}

		leftHalf=mergeSort(leftHalf,0, leftHalf.length-1);
		rightHalf=mergeSort(rightHalf,0,rightHalf.length-1);

		return merge(leftHalf,rightHalf);
	}
	
	public static void findTriplets(int[] array,int number){
		array=mergeSort(array, 0, array.length-1);
		for(int i=0;i<array.length-2;i++){
			int remains=number-array[i];
			int j=i+1;
			int last=array.length-1;
			while(j<last){
				if(array[j]+array[last]==remains){
					System.out.print("Found triplet: "+"("+array[i]+","+array[j]+","+array[last]+")"+" ");
					System.out.println();
					last--;
				}
				else if(array[i]+array[last]<remains)
					j++;
				else
					last--;
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={4,7,5,1,2,3,6};
		findTriplets(a, 11);
	}

}
