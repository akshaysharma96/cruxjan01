package Practice2;

public class IntersectionOptimized {

	/**
	 * Find the intersection of two arrays in minimum time possible
	 */
	
	public static void findIntersection(int[] arr1, int[] arr2){
		int smaller[],larger[];
		smaller=arr1.length>arr2.length? arr2:arr1;
		larger=arr1.length>arr2.length? arr1:arr2;
		smaller=mergeSort(smaller, 0,smaller.length-1);
		for(int i=0;i<larger.length;i++){
			binarySearch(smaller, larger[i]);
		}
	}
	public static int[] merge(int[] arr1, int[] arr2){
		int[] result=new int[arr1.length+arr2.length];
		int i=0;int j=0;int k=0;
		while(i<arr1.length&&j<arr2.length){
			if(arr1[i]<arr2[j]){
				result[k++]=arr1[i++];
			}
			else{
				result[k++]=arr2[j++];
			}
		}
		
		while(i<arr1.length){
			result[k++]=arr1[i++];
		}
		while(j<arr2.length){
			result[k++]=arr2[j++];
		}
		
		return result;
	}
	
	
	public static int[] mergeSort(int[] arr, int startIndex, int lastIndex){
		if(startIndex>=lastIndex){
			return arr;
		}
		int[] leftHalf= new int[arr.length/2];
		int[] rightHalf = new int[arr.length-leftHalf.length];
		int i=0;int j=0;
		for(i=0;i<leftHalf.length;i++){
			leftHalf[i]=arr[i];
		}
		
		for(j=0;i<arr.length;i++,j++){
			rightHalf[j]=arr[i];
		}
		
		leftHalf=mergeSort(leftHalf,0, leftHalf.length-1);
		rightHalf=mergeSort(rightHalf,0,rightHalf.length-1);
		
		return merge(leftHalf,rightHalf);
	}
	
	public static void binarySearch(int[] arr, int number){
		int start=0;int last=arr.length-1;
		int mid;
		while(start<=last){
			mid=(start+last)/2;
			if(arr[mid]==number){
				System.out.print("Found intersection: "+"("+arr[mid]+","+number+")");
				System.out.println();
				return;
			}
			if(number>arr[mid]){
				start=mid+1;
				last=last;
			}
			else if(number<arr[mid]){
				start=start;
				last=mid-1;
			}
		}
		System.out.println("No element found");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr1={52,3112,42,11,323,454,121,66,9383,9387238,23982,3234,123,4354,11,2313,3132,98,4542,212};
		int[] arr2={434,55,244,567,3,213,44,66,233,575};
		findIntersection(arr1, arr2);
	}

}
