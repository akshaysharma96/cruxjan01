package Practice2;

public class PowerOfX {

	/**
	 * Find the nth power of x using recursion with optimized solution
	 */
	public static int findPower(int x, int n){
		if(n==0){
			return 1;
		}
		if(n==1){
			return x;
		}
		
		int ans=findPower(x, n-1);
		return ans*x;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(findPower(6, 3));
	}

}
