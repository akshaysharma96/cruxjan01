package Practice2;

public class RemoveDuplicatesString {

	/**
	 * Remove duplicates from a string 
	 */
	public static String removeDuplicates(String str){
		String outString="";
		for(int i=0;i<str.length();){
			if(i==str.length()-1){
				outString=outString+str.charAt(i);
				break;
			}
			
			if(str.charAt(i)==str.charAt(i+1)){
				outString=outString+str.charAt(i);
				i=i+2;
			}
			else{
				outString=outString+str.charAt(i);
				i++;
			}
		}
		return outString;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a="aabbcceeeddd";
		a=removeDuplicates(a);
		System.out.println(a);
	}

}
