package Practice2;

public class EvaluatePolynomial {

	/**
	 * Evalute the value of a polynomial in O(n)
	 */
	public static int evaluatePolynomial(int[] arr, int number){
		if(arr.length==0){
			return 0;
		}
		int value=number;
		int sum=arr[arr.length-1];
		for(int i =arr.length-2;i>=0;i--){
			sum=sum+arr[i]*value;
			value=value*number;
		}
		
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
