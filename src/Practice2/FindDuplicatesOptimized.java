package Practice2;

public class FindDuplicatesOptimized {

	/**
	 * Check whether duplicates exist is an array
	 */
	public static int[] merge(int[] arr1, int[] arr2){
		int[] result=new int[arr1.length+arr2.length];
		int i=0;int j=0;int k=0;
		while(i<arr1.length&&j<arr2.length){
			if(arr1[i]<arr2[j]){
				result[k++]=arr1[i++];
			}
			else{
				result[k++]=arr2[j++];
			}
		}
		
		while(i<arr1.length){
			result[k++]=arr1[i++];
		}
		while(j<arr2.length){
			result[k++]=arr2[j++];
		}
		
		return result;
	}
	
	
	public static int[] mergeSort(int[] arr, int startIndex, int lastIndex){
		if(startIndex>=lastIndex){
			return arr;
		}
		int[] leftHalf= new int[arr.length/2];
		int[] rightHalf = new int[arr.length-leftHalf.length];
		int i=0;int j=0;
		for(i=0;i<leftHalf.length;i++){
			leftHalf[i]=arr[i];
		}
		
		for(j=0;i<arr.length;i++,j++){
			rightHalf[j]=arr[i];
		}
		
		leftHalf=mergeSort(leftHalf,0, leftHalf.length-1);
		rightHalf=mergeSort(rightHalf,0,rightHalf.length-1);
		
		return merge(leftHalf,rightHalf);
	}
	
	
	public static boolean checkDuplicates(int[] arr){
		boolean is_duplcate=false;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==arr[i+1]){
				is_duplcate=true;
				break;
			}
		}
		return is_duplcate;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr={67,45,42,27,14,10,7,27,2};
		arr=mergeSort(arr, 0, arr.length-1);
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		
		System.out.println(checkDuplicates(arr));
	}

}
