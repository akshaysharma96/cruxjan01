package GeeksForGeeks;

import java.util.Scanner;

import BinaryTree.BinaryTree;
import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;


public class InorderSuccessor {
	public static void getInorderSuccessor(BinaryTree<Integer> root,int element,BinaryTree<Integer> pre,BinaryTree<Integer> suc){
		if(root==null)
			return;
		
		if(root.data==element){
			if(root.left!=null){
				BinaryTree<Integer> leftChild=root.left;
				BinaryTree<Integer> temp=leftChild;
				while(temp.right!=null){
					temp=temp.right;
				}
				pre=temp;
			}
			if(root.right!=null){
				BinaryTree<Integer> rightChild=root.right;
				BinaryTree<Integer>temp=rightChild;
				while(temp.left!=null){
					temp=temp.left;
				}
				suc=temp;
			}
			System.out.println(pre.data+" "+suc.data);
			return;
		}
		
		if(element>root.data){
			suc=root;
			getInorderSuccessor(root.right, element, pre, suc);
		}
		else{
			pre=root;
			getInorderSuccessor(root.left, element, pre, suc);
		}
	}
	public static void main(String []args) throws QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		BinaryTree<Integer> pre=null,suc=null;
		getInorderSuccessor(root, 25, pre, suc);
//		System.out.println(pre.data+" "+suc.data);
	}
	
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}
}
