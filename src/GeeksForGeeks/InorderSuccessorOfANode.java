package GeeksForGeeks;

import java.util.Scanner;

import BinaryTree.BinaryTree;
import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class InorderSuccessorOfANode {

	public static void main(String[]args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		BinaryTree<Integer> succ=findInorderSuccessor(root,17);
		System.out.println(succ.data);
	}
	private static BinaryTree<Integer> findInorderSuccessor(BinaryTree<Integer> root,int element) {
		if(root.data==element){
			BinaryTree<Integer> temp=root.right;
			while(temp.left!=null){
				temp=temp.left;
			}
			return temp;
		}
		if(root.data>element){
			return findInorderSuccessor(root.left, element);
		}
		else{
			return findInorderSuccessor(root.right, element);
		}
		
	}
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;


	}
}
