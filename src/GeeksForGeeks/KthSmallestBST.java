package GeeksForGeeks;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

import BinaryTree.BinaryTree;
import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class KthSmallestBST {


	public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;


	}
	public static void main(String[] args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		int k=kthsmallest(root,3);
		System.out.println(k);
	}
	private static int kthsmallest(BinaryTree<Integer> root,int k) {
		ArrayList<Integer>arr=new ArrayList<>();
		kthsmallest(root,arr);
		return arr.get(k-1);
	}
	private static void kthsmallest(BinaryTree<Integer> root,ArrayList<Integer> arr) {
		if(root==null)
			return;
		
		kthsmallest(root.left, arr);
		arr.add(root.data);
		kthsmallest(root.right, arr);
	}
}
