package GeeksForGeeks;

import java.util.Scanner;

import L17.QueueEmptyException;
import StackAndQueues.Queue;
import BinaryTree.BinaryTree;

public class RemoveOutsideRange {

	public static int findInorderSuccessor(BinaryTree<Integer> root){
		BinaryTree<Integer> temp=root.right;
		while(temp.left!=null){
			temp=temp.left;
		}
		int returnValue= temp.data;
		temp=null;
		return returnValue;
	}

	public static void deleteKeys(BinaryTree<Integer> root,int min,int max){
		if(root==null)
			return;

		if(root.data<min||root.data>max){
			if(root.left==null&&root.left==null){
				root=null;
				return;
			}
			else if(root.left!=null){
				int inorder=findInorderSuccessor(root);
				root.data=inorder;
			}
			
			else{
				int inorder=findInorderSuccessor(root);
				root.data=inorder;
			}
		}
		deleteKeys(root.left, min, max);
		deleteKeys(root.right, min, max);
	}

	public static void main(String[]args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		deleteKeys(root,2, 42);
		printTree(root);
	}
		private static void printTree(BinaryTree<Integer> root) {
		if(root==null)
			return;
		
		printTree(root.left);
		System.out.print(root.data+" ");
		printTree(root.right);
	}

		public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
			Scanner sc= new Scanner(System.in);
			Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
			System.out.print("Enter the value of the root node: ");
			int data=sc.nextInt();
			if(data==-1||data<0){
				return null;
			}
			BinaryTree<Integer> root=new BinaryTree<Integer>(data);
			q.enqueue(root);
			root.size++;
			while(!q.isEmpty()){
				BinaryTree<Integer> dequequed=q.dequeue();
				int leftNode,rightNode;
				System.out.print("Enter the value of the left node of "+dequequed.data+": ");
				leftNode=sc.nextInt();
				if(leftNode!=-1&&leftNode>=0){
					BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
					root.size++;
					dequequed.left=leftTree;
					q.enqueue(leftTree);
				}
				System.out.print("Enter the value of the right node of "+dequequed.data+": ");
				rightNode=sc.nextInt();
				if(rightNode!=-1&&rightNode>=0){
					BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
					dequequed.right=rightTree;
					root.size++;
					q.enqueue(rightTree);
				}
			}
			return root;


		}
	}
