package GeeksForGeeks;

import java.util.Scanner;

import BinaryTree.BinaryTree;
import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class SumLarger {
	
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;


	}
	
	public static void main(String[]args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		fillLarger(root, 0);
		inorder(root);
	}
	
	public static void inorder(BinaryTree<Integer> root){
		if(root==null)
			return;
		inorder(root.left);
		System.out.print(root.data+" ");
		inorder(root.right);
	}
	
	public static int fillLarger(BinaryTree<Integer> root,int sum){
		if(root==null)
			return sum+0;
		sum=fillLarger(root.right, sum);
		sum=root.data+sum;
		root.data=sum;
		return fillLarger(root.left, sum);
	}
}
