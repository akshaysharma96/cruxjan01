package GeeksForGeeks;

import java.util.Scanner;

import BinaryTree.BinaryTree;
import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class KeysBetweenRange {
	
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;


	}
	public static void main(String[]args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		findKeysInRange(root,2,45);
	}
	private static void findKeysInRange(BinaryTree<Integer> root, int i, int j) {
		if(root==null)
			return;
		
		if(root.data>i&&root.data>j)
			findKeysInRange(root.left, i, j);
		
		else if(root.data<i&&root.data<j)
			findKeysInRange(root.right, i, j);
		
		else{
			System.out.print(root.data+" ");
			findKeysInRange(root.left, i, j);
			findKeysInRange(root.right, i, j);
		}
	}
}
