package GeeksForGeeks;

public class SortedArrayTree {
	
	
	public static void printSortedBST(int[] arr,int start, int end){
		if(start>end){
			return;
		}
		
		printSortedBST(arr, start*2+1, end);
		System.out.print(arr[start]+" ");
		printSortedBST(arr, start*2+2, end);
	}
	public static void main(String[]args){
		int[]arr={4,2,5,1,3};
		printSortedBST(arr,0,arr.length-1);
	}
}
