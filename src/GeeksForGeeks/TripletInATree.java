package GeeksForGeeks;

import java.util.ArrayList;
import java.util.Scanner;

import BinaryTree.BinaryTree;
import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class TripletInATree {

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;


	}
	
	public static void isTriplet(BinaryTree<Integer> root,ArrayList<Integer> arr){
		if(root==null)
			return;
		isTriplet(root.left,arr);
		arr.add(root.data);
		isTriplet(root.right,arr);
	}

	public static void main(String[]args) throws QueueEmptyException, Lecture15Queue.QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		ArrayList<Integer> arr=new ArrayList<>();
//		isTriplet(root);
		LL ll=BSTtoLL(root);
		printLL(ll.head);
	}
	
	private static void isTriplet(BinaryTree<Integer> root) {
		ArrayList<Integer> arr=new ArrayList<>();
		isTriplet(root,arr);
		System.out.println(arr);
		for(int i=0;i<arr.size()-2;i++){
			int a=arr.get(i);
			int b=i+1;
			int c=arr.size()-1;
			while(b<c){
				if(a+b+c==0){
					System.out.println("Triplet exists");
					b++;
					c--;
					return;
				}
				if(a+b+c>0){
					c--;
				}
				if(a+b+c<0){
					b++;
				}
			}
		}
	}

	public static LL BSTtoLL(BinaryTree<Integer> root){
		if(root==null){
			LL ll = new LL();
			return ll;
		}
		LL linkedList=BSTtoLL(root.left);
		Node node=new Node(root.data);
		if(linkedList.head==null){
			linkedList.head=node;
			linkedList.tail=node;
		}
		else{
			linkedList.tail.next=node;
			linkedList.tail=node;
		}
		linkedList.tail.next=BSTtoLL(root.right).head;
		
		return linkedList;
	}
	
	public static void printLL(Node head){
		Node temp=head;
		while(temp!=null){
			System.out.print(temp.data+"-->");
			temp=temp.next;
		}
	}
	
}


class Node{
	int data;
	Node next;
	
	public Node(int data) {
		// TODO Auto-generated constructor stub
		this.data=data;
	}
}

class LL{
	Node head;
	Node tail;
	
	LL(){
		head=null;
		tail=null;
	}
}
