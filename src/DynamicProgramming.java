
public class DynamicProgramming {

	
	public static int LCSBruteForce(String s, String t){
		if(s.length()==0||t.length()==0){
			return 0;
		}
		if(s.charAt(0)==t.charAt(0)){
			return LCSBruteForce(s.substring(1), t.substring(1));
		}
		else{
			int ans1=LCSBruteForce(s, t.substring(1));
			int ans2=LCSBruteForce(s.substring(1), t);
			return Math.max(ans1, ans2);
		}
	}
	
	public static int editDistance(String s,String t){
		if(s.length()==0&&t.length()==0){
			return 0;
		}
		if(s.length()==0){
			return t.length();
		}
		if(t.length()==0){
			return s.length();
		}
		
		if(s.charAt(0)==t.charAt(0)){
			return editDistance(s.substring(1), t.substring(1));
		}
		
		int i=1+editDistance(s, t.substring(1));
		int d=1+editDistance(s.substring(1), t);
		int s2=1+editDistance(s.substring(1), t.substring(1));
		
		
		return Math.min(i,Math.min(d, s2));
	}
	
	public static int editDistanceDP(String s,String t){
		int[][] arr = new int[s.length()+1][t.length()+1];
		for(int i=0;i<=s.length();i++){
			for(int j=0;j<=t.length();j++){
				arr[i][j]=-1;
			}
		}
		return editDistanceDP(s,t,arr);
	}
	
	 private static int editDistanceDP(String s, String t, int[][] arr) {
		// TODO Auto-generated method stub
		 int m= s.length();
		 int n=t.length();
		if(m==0&&n==0){
			arr[m][n]=0;
			return arr[m][n];
		}
		if(n==0){
			arr[m][n]=m;
			return arr[m][n];
		}
		if(m==0){
			arr[m][n]=n;
			return arr[m][n];
		}
		
		if(arr[m][n]!=-1){
			return arr[m][n];
		}
		
		if(s.charAt(0)==t.charAt(0)){
			return editDistanceDP(s.substring(1), t.substring(1),arr);
		}
		
		int i=1+editDistanceDP(s, t.substring(1),arr);
		int d=1+editDistanceDP(s.substring(1), t,arr);
		int s2=1+editDistanceDP(s.substring(1), t.substring(1),arr);
		
		
		arr[m][n]=Math.min(i,Math.min(d, s2));
		return arr[m][n];
		
	}
	 
	 public static int editDistanceDPIterative(String s,String t){
		 int m=s.length()+1;
		 int n=t.length()+1;
		 int s_l=s.length();
		 int t_l=t.length();
		 int[][] arr = new int[m][n];
		 arr[0][0]=0;
		 for(int i=1;i<m;i++){
			 arr[i][0]=i;
		 }
		 for(int i=1;i<n;i++){
			 arr[0][i]=i;
		 }
		 for(int i=1;i<m;i++){
			 for(int j=1;j<n;j++){
				 if(s.charAt(s_l-i)==t.charAt(t_l-j)){
					 arr[i][j]=arr[i-1][j-1];
				 }else{
					 int insert=1+arr[i][j-1];
					 int sub=1+arr[i-1][j-1];
					 int del=1+arr[i-1][j];
					 arr[i][j]=Math.min(insert, Math.min(sub,del));
				 }
			 }
		 }
		 return arr[m-1][n-1];
	 }
	 
	public static void main(String[] args) {
		System.out.println(editDistanceDPIterative("abcxy", "bxy"));
	}
}
