import java.util.Scanner;


public class Asingment1Pattern1 {

	/**
	 * @param args
	 */
	/*
	 * Display pattern like: 
	 * 1
	 * 11
	 * 101
	 * 1001
	 * 10001
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		N=sc.nextInt();
		int rows=1;
		while(rows<=N){
			int col=1;
			while(col<2){
				System.out.print(1);
				col++;
			}
			col=1;
			while(col<=rows-2){
				System.out.print(0);
				col++;
			}
			col=1;
			int count=1;
			if(rows>1){
				count=2;
			}
			while(col<count){
				System.out.print(1);
				col++;
			}
			System.out.println();
			rows++;
		}
		
	}

}
