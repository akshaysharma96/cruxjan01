package HashMapOwn;

import java.util.ArrayList;

public class HashMap<K,V> {

	private ArrayList<Node<K, V>> bucketList; //N for the load factor
	private int size; //K for load factor

	public HashMap() {
		bucketList=new ArrayList<Node<K,V>>();
		size=0;
		for(int i=0;i<10;i++){
			bucketList.add(null);
		}
	}


	private int getIndex(K key){
		int hash=key.hashCode();
//		System.out.println("hashcode: "+hash);
		return hash%bucketList.size();
	}

	public void put(K key,V value){
		int index=getIndex(key);
//		System.out.println("Index: "+index);
		Node<K,V>node =bucketList.get(index);
		if(node==null){
			Node<K, V> newNode=new Node<K, V>(key, value);
			bucketList.set(index,newNode);
			size++;
//			System.out.println("Size: "+size);
			return;
		}
		Node<K, V> newNode=new Node<K,V>(key, value);
		newNode.next=node;
		node=newNode;
		bucketList.set(index, node);
		size++;
		
		double loadFactor=(size*1.0)/bucketList.size();
//		System.out.println(loadFactor);
		if(loadFactor>0.7){
			rehash();
		}

	}


	private void rehash() {
		ArrayList<Node<K, V>> temp=bucketList;
		bucketList=new ArrayList<Node<K, V>>();
		for(int i=0;i<2*temp.size();i++){
			bucketList.add(null);
		}
		size=0;
		for(int i=0;i<temp.size();i++){
			//Get node at index i
			Node<K, V> tempHead=temp.get(i);
			if(tempHead!=null){
				//traverse the whole linkedlist to put that linkedlist to new bucketList
				put(tempHead.key, tempHead.value);
				tempHead=tempHead.next;
			}
		}
		
	}


	public void remove(K key) throws KeyErrorException{
		int index=getIndex(key);

		Node<K, V> head=bucketList.get(index);

		if(head==null){
			throw new KeyErrorException();
		}
		Node<K,V> temp=head,previous=null;
		while(temp!=null){
			if(temp.key.equals(key)){
				if(previous==null){
					bucketList.set(index,head.next);
					size--;
					return;
				}
				else{
					previous.next=temp.next;
					temp.next=null;
					size--;
					return;
				}
			}
			previous=temp;
			temp=temp.next;
		}
		throw new KeyErrorException();

	}

	public V get(K key) throws KeyErrorException{
		int index=getIndex(key);
//		System.out.println("Index get: "+index);
		Node<K, V> head=bucketList.get(index);
		if(head==null)
			throw new KeyErrorException();

		Node<K, V> temp=head;
		V value=null;
		while(temp!=null){
			if(temp.key.equals(key)){
				value=temp.value;
				return value;
			}
			temp=temp.next;
		}
		throw new KeyErrorException();
	}
	

}
