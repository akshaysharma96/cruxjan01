
public class LargestPalindrome {

	public static int largestPalindrome(String str){

		if(str.length()==1){
			return 1;
		}
		if(str.length()==2&&(str.charAt(0)==str.charAt(1))){
			return 2;
		}
		else{
			int l=str.length();
			if(str.charAt(0)==str.charAt(l-1)){
				return 2+largestPalindrome(str.substring(1,l-1));
			}
			else{
				return Math.max(largestPalindrome(str.substring(0,l-1)),largestPalindrome(str.substring(1,l)));
			}
		}
	}

	public static int largestPalindrome(String str,int start,int end){
		int[][] arr=new int[end][end];
		for(int i=0;i<end;i++){
			for(int j=0;j<end;j++){
				arr[i][j]=-1;
			}
		}
		for(int i=0;i<end;i++){
			for(int j=0;j<end;j++){
				if(i==j){
					arr[i][j]=1;
				}
			}
		}
		for(int i=0;i<end;i++){
			for(int j=0;j<end;j++){
				if(j==i+1&&j<end){
					if(str.charAt(i)==str.charAt(j))
						arr[i][j]=2;
				}
			}
		}

		return largestPalindrome(str,0,end-1,arr);

	}

	private static int largestPalindrome(String str, int i, int end, int[][] arr) {
		if(arr[i][end]!=-1){
			return arr[i][end];
		}
		else{
			if(str.charAt(i)==str.charAt(end)){
				arr[i][end]=2+largestPalindrome(str, i+1, end-1, arr);
			}
			else
				arr[i][end]=Math.max(largestPalindrome(str, i+1, end, arr), largestPalindrome(str, i, end-1, arr));

		}
		
		return arr[i][end];
	}

	public static void main(String[] args) {
		String str="BBABCBCABKMNAAADNVCNKLAA";
//		System.out.println(largestPalindrome(str));
		System.out.println(largestPalindrome(str, 0,str.length()));
	}
}
