package Lecture12;

import java.util.Scanner;

public class LinkedListUse {
	/*
	 * Function that creates a Linked List and returns the head of the list
	 * 
	 * */
	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;		//maintain tail and head
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {		//checking whether head was null or not
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}

	public static void printLL(Node<Integer> head) {
		Node<Integer> temp = head;			//better to keep a temporary reference for better code quality
		while(temp != null) {
			System.out.print(temp.data + "->");
			temp = temp.next;
		}
		System.out.println();

	}

	public static void printElement(Node<Integer> head,int index){
		Node<Integer> temp=head;
		int i=1;
		while(i<=index&&temp!=null){
			if(i==index){
				System.out.println("Data at the "+index+"th"+" index is: "+temp.data);
				return;
			}
			i++;
			temp=temp.next;
		}

		System.out.println("Element Not found");
	}

	public static Node<Integer> insertAt(Node <Integer> head, int value, int index){
		if(index<=0){
			System.out.println("Index starting from 1");
			return head;
		}

		if(head==null){
			Node<Integer> newNode=new Node(value);
			newNode.next=null;
			head=newNode;
			return head;
		}

		if(index==1){
			Node<Integer> newNode = new Node(value);
			newNode.next=head;
			head=newNode;
			return head;
		}

		int i=1;
		Node<Integer> temp=head;
		while(i<=index&&temp!=null){
			if(i==index-1){
				Node<Integer> newNode = new Node(value);
				newNode.next=temp.next;
				temp.next=newNode;
				break;
			}
			i++;
			temp=temp.next;
		}
		if(temp==null){
			System.out.println("Invalid index");
		}

		return head;
	}


	public static int getLength(Node<Integer> head){
		if(head==null)
			return 0;

		int length=1; 
		Node<Integer> temp=head;
		while(temp.next!=null){
			temp=temp.next;
			length++;
		}

		return length;
	}

	public static Node<Integer> deleteElement(Node<Integer> head,int index){
		if(head==null){
			System.out.println("Empty list cannot delete element");
			return head;
		}
		if(index==1){
			head=head.next;
			return head;
		}
		int i=1;
		Node<Integer> temp=head;
		while(i!=index-1&&temp!=null){
			i++;
			temp=temp.next;
		}
		if(temp==null||temp.next==null){
			System.out.println("Length less than index, element cannot be removed");
			return head;
		}

		Node<Integer> nodeToRemove=temp.next;
		temp.next=nodeToRemove.next;
		return head;
	}

	static Node<Integer> insertRecursive(Node<Integer> head, int index, int value){
		if(head==null){
			System.out.println("Empty list or not a valid index");
			return head;
		}
		else if(index==0){
			Node<Integer> newNode = new Node(value);
			newNode.next=head;
			head=newNode;
			return head;
		}

		if(index==1){
			Node<Integer> newNode= new Node(value);
			newNode.next=head.next;
			head.next=newNode;
			return head;
		}

		return insertRecursive(head.next, --index, value);
	}

	static Node<Integer> insertOptimized(Node<Integer> head, int index,int value){
		if(head==null){
			System.out.println("Invalid index or empty list");
		}
		if(index==0){
			Node<Integer> newNode= new Node(value);
			newNode.next=head;
			head=newNode;
			return head;
		}

		head.next=insertOptimized(head.next, --index, value);
		return head;
	}

	static Node<Integer> deleteOptimized(Node<Integer> head, int index, int value){
		if(head==null){
			System.out.println("Invalid index or empty");
		}
		if(index==0){
			return head.next;
		}

		head.next=deleteOptimized(head.next, --index, value);
		return head;
	}

	static Node<Integer> deleteRecursive(Node<Integer> head, int index, int value){
		if(head==null){
			System.out.println("Invlaid index or empty list");
			return head;
		}
		if(index==0){
			head=head.next;
			return head;
		}
		if(index==1){
			Node<Integer> nodeToRemove=head.next;
			head.next=nodeToRemove.next;
			return head;
		}

		return deleteRecursive(head.next, --index, value);
	}

	static int getLengthRecursive(Node<Integer> head){
		if(head==null){
			return 0;
		}
		if(head.next==null){
			return 1;
		}

		return 1+getLength(head.next);
	}
	
	static int findElement(Node<Integer> head, int value){
		if(head==null){
			System.out.println("List is empty");
			return -1;
		}
		int i=0;
		Node<Integer> temp=head;
		while(temp!=null){
			if(temp.data==value){
				System.out.println("Element found");
				return i;
			}
			i++;
			temp=temp.next;
		}
		
		System.out.println("Element not found");
		return -1;
	}
	
	static int findElementRecursion(Node<Integer> head,int value){
		if(head==null){
			System.out.println("List empty or element not found");
			return -1;
		}
		if(head.data==value)
			return 0;
		
		return 1+findElementRecursion(head.next, value);
	}
	
	public static void main(String[] args) {
		Node<Integer> head = takeInput();
		printLL(head);
		//printElement(head, 4);
		//insertAt(head, 5,4);
		//printLL(head);
		//System.out.println(getLength(head));
		//deleteElement(head, 4);
		printLL(head);
		insertRecursive(head, 1, 16);
		printLL(head);
		System.out.println(getLengthRecursive(head));
		int i=findElementRecursion(head, 17);
		System.out.println(i);
	}

}
