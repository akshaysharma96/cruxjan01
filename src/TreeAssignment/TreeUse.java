package TreeAssignment;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class TreeUse<T> {

	public static Tree<Integer> takeInput() throws QueueEmptyException{
		Queue<Tree<Integer>> q = new Queue<Tree<Integer>>();
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the root value: ");
		int data=sc.nextInt();
		Tree<Integer> root = new Tree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			Tree<Integer> dequequed = q.dequeue();
			int root_number;
			System.out.print("Enter the number of children of "+dequequed.data+": ");
			root_number=sc.nextInt();
			for(int i=0;i<root_number;i++){
				System.out.print("Enter the "+i+"th child of "+dequequed.data+": ");
				int dataInput=sc.nextInt();
				Tree<Integer> child = new Tree<Integer>(dataInput);
				q.enqueue(child);
				dequequed.children.add(child);
			}
		}
		sc.close();
		return root;
	}

	public static void printTree(Tree<Integer> root){
		String str=root.data+": ";
		for(Tree<Integer> child:root.children){
			str=str+child.data+",";
		}
		System.out.print(str);
		System.out.println();
		for(Tree<Integer> child:root.children)
			printTree(child);
	}

	public static void levelWisePrint(Tree<Integer> root) throws QueueEmptyException{
		Queue<Tree<Integer>> q =  new Queue<Tree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			Tree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+": ";
			for(Tree<Integer> child:dequequed.children){
				q.enqueue(child);
				str=str+child.data+",";
			}
			System.out.println(str);
		}
	}
	
	public static int getNodes(Tree<Integer> root){
		if(root==null)
			return 0;
		int number=1;
		for(Tree<Integer> child:root.children){
			number=number+getNodes(child);
		}
		return number;
	}
	
	
	public static int sumOfNodes(Tree<Integer> root){
		if(root==null)
			return 0;
		int sum=root.data;
		for(Tree<Integer> child:root.children)
			sum=sum+sumOfNodes(child);
		
		return sum;
	}
	
	public static int getHeight(Tree<Integer> root){
		if(root==null){
			return 0;
		}
		int height=0;
		for(Tree<Integer> child:root.children){
			int ans=getHeight(child);
			if(ans>height)
				height=ans;
		}
		return height+1;
	}
	
	public static void nodeAtDepthK(Tree<Integer> root,int k){
		if(k==0){
			System.out.print(root.data+" ");
			return;
		}
		for(Tree<Integer> child:root.children){
			nodeAtDepthK(child, k-1);
		}
	}
	public static int  maxiumNode(Tree<Integer> root){
		if(root==null)
			return Integer.MIN_VALUE;
		int data=root.data;
		for(Tree<Integer> child:root.children){
			int ans=maxiumNode(child);
			if(ans>data)
				data=ans;
		}
		return data;
	}
	
	public static int numberOfLeaves(Tree<Integer> root){
		int number=0;
		if(root.children.size()==0){
			number=1;
		}
		for(Tree<Integer> child:root.children){
			number=number+numberOfLeaves(child);
		}
		return number;
	} 
	
	public static int nodeJustGreaterThan(Tree<Integer> root,int k){
		if(root.data<k){
			int number=Integer.MAX_VALUE;
			for(Tree<Integer> child:root.children){
				int ans=nodeJustGreaterThan(child, k);
				if(ans<number)
					number=ans;
			}
			return number;
		}
		int number=root.data;
		for(Tree<Integer> child:root.children){
			int ans=nodeJustGreaterThan(child, k);
			if(number>ans)
				number=ans;
		}
		return number;
	}
	
	public static Tree<Integer> replaceWithDepth(Tree<Integer> root, int k){
		if(root==null)
			return root;
		
		root.data=k;
		
		for(Tree<Integer> child:root.children){
			replaceWithDepth(child, k+1);
		}
		return root;
	}

	public static void main(String[]args) throws QueueEmptyException{
		Tree<Integer> root=takeInput();
		//printTree(root);
		//levelWisePrint(root);
		//System.out.println(getNodes(root));
		//System.out.println(sumOfNodes(root));
		//System.out.println(getHeight(root));
		//nodeAtDepthK(root, 2);
		//System.out.println(numberOfLeaves(root));
		//System.out.println(nodeJustGreaterThan(root, 5));
		root=replaceWithDepth(root, 0);
		levelWisePrint(root);
	}
}
