package Lecture14;

import java.util.Scanner;

public class MergeLinkedList {

	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;		//maintain tail and head
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {		//checking whether head was null or not
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}

	public static void printLL(Node<Integer> head) {
		Node<Integer> temp = head;			//better to keep a temporary reference for better code quality
		while(temp != null) {
			System.out.print(temp.data + "->");
			temp = temp.next;
		}
		System.out.println();

	}
	
	static Node<Integer> getMidPointForMergeSort(Node<Integer> head){
		Node<Integer> slow,fast;
		slow=head;fast=head;
		while(fast.next!=null&&fast.next.next!=null){
			slow=slow.next;
			fast=fast.next.next;
		}
		return slow;
	}
	
	static Node<Integer> mergeSort(Node<Integer> head){
		if(head.next==null||head==null){
			return head;
		}
		Node<Integer> midPoint =getMidPointForMergeSort(head);
		Node<Integer> secondHead=midPoint.next;
		midPoint.next=null;
		
		head=mergeSort(head);
		secondHead=mergeSort(secondHead);
		head= mergeLinkedList(head,secondHead);
		
		return head;
	}
	static Node<Integer> mergeLinkedList(Node <Integer> headA, Node<Integer> headB){
		Node<Integer> head,tail;
		/*
		 * Condition when one or both of the lists are empty
		 * */
		if(headA==null&&headB==null){
			return headA;
		}
		
		if(headA==null){
			return headB;
		}
		
		if(headB==null){
			return headA;
		}
		
		if(headA.data<headB.data){
			head=headA;
			tail=headA;
			headA=headA.next;
		}else{
			head=headB;
			tail=headB;
			headB=headB.next;
		}

		while(headA!=null&&headB!=null){
			if(headA.data<headB.data){
				tail.next=headA;
				tail=headA;
				headA=headA.next;
			}
			else{
				tail.next=headB;
				tail=headB;
				headB=headB.next;
			}
		}
		if(headA==null){
			tail.next=headB;
		}
		else{
			tail.next=headA;
		}
		
		return head;
	}
	
	public static void main(String[]args){
		Node<Integer> head1=takeInput();
		head1=mergeSort(head1);
		printLL(head1);
//		Node<Integer> head2=takeInput();
//		
//		Node<Integer> head3=mergeLinkedList(head1, head2);
		
	}
}	
