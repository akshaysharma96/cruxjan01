package Recursion1;

public class RemoveX {

	/**
	 * "xabxabcx" to "ababc"
	 */
	
	public static String removeX(String str,int index){
		if(index==str.length()){
			return "";
		}
		if(str.charAt(index)=='x'){
			return ""+removeX(str, index+1);
		}
		else{
			return String.valueOf(str.charAt(index))+removeX(str, index+1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str ="akshayxsharma";
		System.out.println(removeX(str, 0));
	}

}
