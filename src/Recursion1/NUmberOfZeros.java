package Recursion1;

public class NUmberOfZeros {

	/**
	 * Find the number of zeros in a number
	 */
	public static int numberOfZeros(int number,int count){
		if(number%(Math.pow(10,count))!=0){
			return count-1;
		}
		return numberOfZeros(number, ++count);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zeros=numberOfZeros(673020000,0);
		System.out.println(zeros);
	}

}
