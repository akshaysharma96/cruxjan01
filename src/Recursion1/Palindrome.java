package Recursion1;

public class Palindrome {

	/**
	 * Check if a String is a palindrome using Recursion
	 */
	public static boolean isPalindrome(String str, int i){
		if(i==(str.length()-1)/2){
			if(str.charAt(i)==str.charAt(str.length()-1-i))
				return true;
			else
				return false;
		}
		else{
			if(str.charAt(i)!=str.charAt(str.length()-1-i)){
				return false;
			}
			else{
				return isPalindrome(str, i+1);
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPalindrome("akshay", 0));
	}

}
