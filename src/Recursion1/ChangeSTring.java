package Recursion1;

public class ChangeSTring {

	/**
	 * "hello" to  "hel*lo"
	 */
	public static String convertString(String str, int length){
		if(length==0){
			return "";
		}
		if(length==1){
			return String.valueOf(str.charAt(0));
		}
		else{
			if(str.charAt(0)==str.charAt(1)){
				return String.valueOf(str.charAt(0))+"*"+convertString(str.substring(1,length), length-1);
			}
			else{
				return String.valueOf(str.charAt(0))+convertString(str.substring(1,length), length-1);
			}
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(convertString("1",1));
	}

}
