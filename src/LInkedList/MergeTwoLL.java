package LInkedList;

import java.util.Scanner;

public class MergeTwoLL {
	
	public static Node<Integer> takeInput(){
		Scanner sc = new Scanner(System.in);
		int input,count;
		System.out.print("Enter the head node of linked list: ");
		input=sc.nextInt();
		Node<Integer> head = new Node<Integer>(input);
		Node<Integer>tail=head;
		System.out.print("Press any key other than -1 to enter next node: ");
		count=sc.nextInt();
		while(count!=-1){
			System.out.print("Enter node: ");
			input=sc.nextInt();
			Node<Integer> node=new Node<Integer>(input);
			tail.next=node;
			tail=node;
			System.out.print("Press any key other than -1 to enter next node: ");
			count=sc.nextInt();
		}
		
		return head;
	}
	
	public static void main(String[]args){
		Node<Integer> head=takeInput();
//		Node<Integer> head2=takeInput();
//		Node<Integer> head=merge(head1,head2);
		head=mergeSort(head);
		printLL(head);
	}

	private static void printLL(Node<Integer> head) {
		// TODO Auto-generated method stub
		if(head==null){
			return;
		}
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"-->");
			temp=temp.next;
		}
	}

	private static Node<Integer> merge(Node<Integer> head1, Node<Integer> head2) {
		// TODO Auto-generated method stub
		if(head1==null&&head2==null)
			return null;
		if(head1==null)
			return head2;
		if(head2==null)
			return head1;
		
		Node<Integer> newHead=null,tail=null;
		Node<Integer> temp1=head1,temp2=head2;
		while(temp1!=null&&temp2!=null){
			if(temp1.data<temp2.data){
				Node<Integer> newNode=new Node<Integer>(temp1.data);
				if(newHead==null){
					newHead=newNode;
					tail=newHead;
				}
				else{
					tail.next=newNode;
					tail=newNode;
				}
				temp1=temp1.next;
			}
			else{
				Node<Integer> newNode=new Node<Integer>(temp2.data);
				if(newHead==null){
					newHead=newNode;
					tail=newHead;
				}
				else{
					tail.next=newNode;
					tail=newNode;
				}
				
				temp2=temp2.next;
			}
		}
		
		if(temp1==null){
			tail.next=temp2;
		}
		if(temp2==null){
			tail.next=temp1;
		}
		
		return newHead;
	}
	
	public static Node<Integer> mergeSort(Node<Integer> head){
		if(head==null||head.next==null){
			return head;
		}
		
		Node<Integer> slow=head,fast=head;
		while(fast.next!=null&&fast.next.next!=null){
			slow=slow.next;
			fast=fast.next.next;
		}
		Node<Integer> lowerHalf=slow.next;
		slow.next=null;
		
		head=mergeSort(head);
		lowerHalf=mergeSort(lowerHalf);
		
		Node<Integer> result=merge(head,lowerHalf);
		
		return result;
	}
}
