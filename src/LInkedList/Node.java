package LInkedList;

import java.util.Scanner;

public class Node<T> {
	public T data;
	public Node<T> next;
	
	public Node(T data) {
		this.data=data;
	}
	
	public static Node<Integer> takeInput(){
		Scanner sc = new Scanner(System.in);
		Node<Integer> head,tail=null;
		head=tail;
		int input,count;
		System.out.print("Enter the head node of linked list: ");
		input=sc.nextInt();
		head.data=input;
		tail=head;
		System.out.print("Press any key other than -1 to enter next node: ");
		count=sc.nextInt();
		while(count!=-1){
			System.out.print("Enter node: ");
			input=sc.nextInt();
			Node<Integer> node=new Node<Integer>(input);
			tail.next=node;
			tail=node;
			System.out.print("Press any key other than -1 to enter next node: ");
			count=sc.nextInt();
		}
		
		return head;
	}
}
