package LInkedList;

import java.util.Scanner;

public class LinkedListUse {
	public static Node<Integer> takeInput(){
		Scanner sc = new Scanner(System.in);
		int input,count;
		System.out.print("Enter the head node of linked list: ");
		input=sc.nextInt();
		Node<Integer> head = new Node<Integer>(input);
		Node<Integer>tail=head;
		System.out.print("Press any key other than -1 to enter next node: ");
		count=sc.nextInt();
		while(count!=-1){
			System.out.print("Enter node: ");
			input=sc.nextInt();
			Node<Integer> node=new Node<Integer>(input);
			tail.next=node;
			tail=node;
			System.out.print("Press any key other than -1 to enter next node: ");
			count=sc.nextInt();
		}
		
		return head;
	}
	
	
	public static Node<Integer> insertFirst(Node <Integer> head,int data){
		if(head==null){
			Node<Integer> newNode = new Node<Integer>(data);
			head=newNode;
			return newNode;
		}
		
		Node<Integer> newNode=new Node<Integer>(data);
		newNode.next=head;
		head=newNode;
		return head;
	}
	
	public static Node<Integer> reverseLinkedList(Node<Integer> head){
		if(head==null||head.next==null)
			return head;
		Node<Integer> temp=head, nextNode=head,prev=null;
		while(temp!=null){
			nextNode=temp.next;
			temp.next=prev;
			prev=temp;
			temp=nextNode;
		}
		
		return prev;
	}
	
	public static void reverseRecursion(Node<Integer> head){
		if(head==null||head.next==null){
			return;
		}
		Node<Integer> rest=head.next;
		if(rest==null){
			return;
		}
		
		reverseLinkedList(rest);
		head.next.next=head;
		head.next=null;
		head=rest;
	}
	
	public static void reversePrint(Node<Integer> head){
		if(head==null){
			return;
		}
		if(head.next==null){
			System.out.print(head.data+"-->");
			return;
		}
		
		reversePrint(head.next);
		System.out.print(head.data+"-->");
	}
	
	public static void printLL(Node<Integer> head){
		if(head==null)
			return;
		Node<Integer> temp=head;
		
		while(temp!=null){
			System.out.print(temp.data+"-->");
			temp=temp.next;
		}
	}
	
	public static Node<Integer> kReverse(Node<Integer> head, int k){
		if(k==0||k==1||head==null||head.next==null)
			return head;
		int count=1;
		Node<Integer>temp=head,prev=null,nextNode=head;
		
		while(temp!=null&&count<=k){
			nextNode=temp.next;
			temp.next=prev;
			prev=temp;
			temp=nextNode;
			count++;
		}
		head.next=kReverse(temp, k);
		return prev;
		
	}
	public static void main(String[] args){
		Node<Integer> head=takeInput();
		head=kReverse(head, 3);
		printLL(head);
	}
	
	public static Node<Integer> swapNodes(Node<Integer> head){
		if(head==null||head.next==null){
			return head;
		}
		
		Node<Integer> temp=head.next;
		head.next=temp.next;
		temp.next=head;
		head.next=swapNodes(head.next);
		
		return temp;
	}
	
	public static Node<Integer> movenNodes(Node<Integer> head,int n){
		Node<Integer> temp1=head,temp2=head;
		if(head==null||head.next==null){
			return head;
		}
		int count=1;
		while(count<=n){
			temp2=temp2.next;
			count++;
		}
		while(temp2.next!=null){
			temp1=temp1.next;
			temp2=temp2.next;
		}
		
		return temp1.next;
	}

}
