package Recursion3;

public class RemoveDuplicates {

	/**
	 * remove duplicates of a string and return string
	 */
	public static String removeDuplicates(String str, String output,int startIndex){
		if(startIndex==str.length()-1){
			return output+String.valueOf(str.charAt(startIndex));
		}
		else{
			if(str.charAt(startIndex)==str.charAt(startIndex+1)){
				output=output+String.valueOf(str.charAt(startIndex));
				return removeDuplicates(str, output, startIndex+2);
			}
			else{
				output=output+String.valueOf(str.charAt(startIndex));
				return removeDuplicates(str, output, startIndex+1);
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s =removeDuplicates("aabbccbadde", "", 0);
		System.out.println(s);
	}

}
