package Recursion3;

public class Q6 {

	/**
	 * xya(fkjd)dsjds answer = fkjd
	 */
	public static String parseBrackets(String str,int startIndex,int lastIndex){
		if(startIndex>=lastIndex){
			return "";
		}
		
		if(str.charAt(startIndex)!='('&&str.charAt(lastIndex)==')'){
			return parseBrackets(str, startIndex+1, lastIndex); 
		}
		
		if(str.charAt(startIndex)=='('&&str.charAt(lastIndex)!=')'){
			return parseBrackets(str, startIndex, lastIndex-1);
		}
		
		if(str.charAt(startIndex)=='('&&str.charAt(lastIndex)==')')
			return str.substring(startIndex+1,lastIndex);
		
		else
			return parseBrackets(str, startIndex+1, lastIndex-1);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(parseBrackets("abc()po", 0, 6));
	}

}
