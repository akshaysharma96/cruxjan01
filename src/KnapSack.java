
public class KnapSack {

	public static int knapsack(int[] wt,int val[],int W){
		int n=wt.length;
		int[][] arr=new int[n+1][W+1];
		for(int i=0;i<=n;i++){
			for(int j=0;j<=W;j++){
				arr[i][j]=-1;
			}
		}
		for(int i=0;i<=n;i++){
			arr[i][0]=0;
		}
		for(int i=0;i<=W;i++){
			arr[0][i]=0;
		}
		
		return knapsack(wt,val,W,n,arr,W);
	}

	
	
	private static int knapsack(int[] wt, int[] val, int w, int n, int[][] arr,
			int W) {
		// TODO Auto-generated method stub
		if(arr[n][w]!=-1){
			return arr[n][w];
		}
		if(wt[n-1]>w){
			arr[n][w]=knapsack(wt, val, w-wt[n-1], n-1, arr, W);
		}
		else{
			arr[n][w]=Math.max(val[n-1]+knapsack(wt, val, w-wt[n-1], n-1, arr, W), knapsack(wt, val, w, n-1, arr, W));
		}
		
		return arr[n][w];
		
	}
	
	public static int iterknapsack(int[] wt,int[] val,int W,int n){
		int[][]arr=new int[n+1][W+1];
		for(int i=1;i<=n;i++){
			for(int j=1;j<=W;j++){
				if(wt[i-1]>j){
					arr[i][j]=arr[i-1][j];
				}else{
					arr[i][j]=Math.max(val[i-1]+arr[i-1][j-wt[i-1]],arr[i-1][j]);
				}
			}
		}
		
		return arr[n][W];
	}



	public static void main(String[] args) {
		int val[] = {60, 100, 120};
	    int wt[] = {10, 20, 30};
	    System.out.println(knapsack(wt, val,50));
	    System.out.println(iterknapsack(wt, val, 150, 3));
	}
}
