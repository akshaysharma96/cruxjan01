import java.util.Scanner;


public class RootsOfQuadraticEquation {

	/**
	 * @param args
	 */
	
	/*
	 *Calculate the nature and roots of an quadratic equation:
	 *                     ax^2+bx+c
	 *                      
	 * Edit: Created a function of the functionality
	 * */
	
	public static void checkRoots(double a, double b, double c){
		double D;
		D=Math.pow(b,2)-4*a*c;
		if(D>0){
			System.out.print("The roots are real and distinct \n");
			D=Math.sqrt(D);
			double root1=(-b+D)/2*a;
			double root2=(-b-D)/2*a;
			System.out.println("The roots are: "+root1+" and "+root2);
			return;
		}
		else if(D==0){
			System.out.print("The roots are real and equal to each other \n");
			double root=-b/2*a;
			System.out.println("The roots are: "+root+" and "+root);
			return;
		}
		else{
			System.out.print("The roots are imaginary");
			return;
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Given the quadratic equation: ax^2+bx+c");
		double a,b,c,D;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the value of a: ");
		a=sc.nextDouble();
		if(a==0){
			System.out.print("Since a=0, the equation is not quadratic");
			return;
		}
		System.out.print("Enter the value of b: ");
		b=sc.nextDouble();
		System.out.print("Enter the value of c: ");
		c=sc.nextDouble();
		checkRoots(a, b, c);
	}

}
