package Practice;

public class sort01 {

	/**
	 * Sort an array of 0s and 1s in one pass only
	 */
	public static void sortArray(int array[]){
		int nextZero=0;
		int temp;
		
		for(int i=0;i<array.length;i++){
			if(array[i]==0){
				temp=array[nextZero];
				array[nextZero]=array[i];
				array[i]=temp;
				nextZero++;
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={1,0,1,0,1,1,0,1,0,0,1,0,1,0,0};
		sortArray(array);
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}

}
