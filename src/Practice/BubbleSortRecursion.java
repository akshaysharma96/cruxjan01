package Practice;

public class BubbleSortRecursion {

	/**
	 * Carry out bubble sort in recursion
	 */

	public static void bubbleSort(int [] a, int lastIndex){
		if(lastIndex<=0){
			return;
		}
		else{
			for(int i=0;i<lastIndex;i++){
				if(a[i]>a[i+1]){
					int temp=a[i];
					a[i]=a[i+1];
					a[i+1]=temp;
				}
			}
			bubbleSort(a, lastIndex-1);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] a ={5,62,13,121,6,9,21};
		bubbleSort(a,a.length-1);
		for(int i=0;i<a.length;i++){
			System.out.print(a[i]+" ");
		}
	}

}
