package Practice;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class CaesersCipher {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String s = in.next();
		int k = in.nextInt();
		String output="";
		for(int i=0;i<s.length();i++){
			if(Character.isLowerCase(s.charAt(i))||Character.isUpperCase(s.charAt(i))){
				if(Character.isLowerCase(s.charAt(i))){
					int asciivalue=(int)s.charAt(i);
					asciivalue=asciivalue-97;
					asciivalue=(asciivalue+k)%26;
					asciivalue=asciivalue+97;
					char mychar=(char)asciivalue;
					output=output+String.valueOf(mychar);
				}
				else{
					int asciivalue=(int)s.charAt(i);
					asciivalue=asciivalue-65;
					asciivalue=(asciivalue+k)%26;
					asciivalue=asciivalue+65;
					char mychar=(char)asciivalue;
					output=output+String.valueOf(mychar);
				}
			}
			else{
				output=output+String.valueOf(s.charAt(i));
			}
		}
		System.out.println(output);

	}
}
