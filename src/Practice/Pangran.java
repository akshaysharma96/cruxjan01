package Practice;
import java.util.*;
public class Pangran {

	/**
	 * Check if a String is a pangram
	 */
	public static boolean isPangram(String str){
		Set<Character> myset= new HashSet<Character>();
		for(char ch='a';ch<='z';ch++){
			myset.add(ch);
		}
		str=str.toLowerCase();
		for(int i=0;i<str.length();i++){
			if(myset.contains(str.charAt(i))){
				myset.remove(str.charAt(i));
			}
		}
		
		if(myset.isEmpty())
			return true;
		
		else
			return false;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="We promptly judged antique ivory buckles for the next prize";
		System.out.println(isPangram(str));
	}

}
