package Practice;

public class IndicesOf7 {

	/**
	 * Find the indices of 7 in an array using recursion only
	 */

	public static int[] findIndices(int[]arr,int index){
		if(index==arr.length){
			int[] output=new int[0];
			return output;
		}
		int[] ans=findIndices(arr, index+1);
		int[] output = new int[0];
		if(arr[index]==7){
			output=new int[ans.length+1];
			output[0]=index;
			for(int i =0;i<ans.length;i++){
				output[i+1]=arr[i];
			}
		}
		else{
			output=new int[ans.length];
			for(int i=0;i<ans.length;i++){
				output[i]=ans[i];
			}
		}

		return output;
	}

	public static int[]  allIndicesOf7(int[] a, int si) {
		if(si == a.length) {
			int[] output = new int[0];
			return output;
		}
		int smallAns[] = allIndicesOf7(a, si+1);

		int[] output = new int[0];

		if(a[si] == 7) {
			output = new int[smallAns.length + 1];
			output[0] = si;
			for(int i = 0; i < smallAns.length; i++) {
				output[i+1] = smallAns[i];
			}
		}
		else {
			output = new int[smallAns.length];
			for(int i = 0; i < smallAns.length; i++) {
				output[i] = smallAns[i];
			}
		}

		return output;
	}

	public static int findLastIndex(int[] array, int index){
		if(index==array.length){
			return -1;
		}
		int i=findLastIndex(array, index+1);
		if(i==-1){
			if(array[index]==7){
				i=index;
			}
		}
		return i;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr={2,4,7,1,8,7,12,7};
		int[] arr1= allIndicesOf7(arr, 0);
		int j=findLastIndex(arr, 0);
		System.out.println(j);
		for(int i=0;i<arr1.length;i++)
			System.out.print(arr1[i]+" ");
	}


}
