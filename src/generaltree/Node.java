package generaltree;

import java.util.ArrayList;

public class Node<T> {
	T data;
	ArrayList<Node<T>> children;
	
	public Node(T data) {
		this.data=data;
		children=new ArrayList<>();
	}
}
