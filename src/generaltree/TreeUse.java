package generaltree;

import java.util.Scanner;

import L17.QueueEmptyException;
import StackAndQueues.Queue;

public class TreeUse {
	
	public static Node<Integer> levelWiseInput() throws Lecture15Queue.QueueEmptyException{
		Queue<Node<Integer>> q = new Queue<Node<Integer>>();
		System.out.print("Enter the root node: ");
		Scanner sc = new Scanner(System.in);
		int data=sc.nextInt();
		Node<Integer> root=new Node<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			Node<Integer> dequequed=q.dequeue();
			System.out.print("Enter the number of nodes of :"+dequequed.data+": ");
			int root_number=sc.nextInt();
			for(int i=0;i<root_number;i++){
				System.out.print("Enter the "+(i+1)+"th child:");
				data=sc.nextInt();
				Node<Integer> node = new Node<Integer>(data);
				q.enqueue(node);
				dequequed.children.add(node);
			}
		}
		
		return root;
	}
	
	public static void printTree(Node<Integer> root) throws Lecture15Queue.QueueEmptyException{
		if(root==null)
			return;
		Queue<Node<Integer>> q = new Queue<>();
		q.enqueue(root);
		while(!q.isEmpty()){
			Node<Integer> deq = q.dequeue();
			System.out.print(deq.data+":");
			for(int i=0;i<deq.children.size();i++){
				System.out.print(deq.children.get(i).data+",");
				q.enqueue(deq.children.get(i));
			}
			System.out.println();
		}
	}
	
	public static int sumOfNodes(Node<Integer> root) throws Lecture15Queue.QueueEmptyException{
		int sum=0;
		if(root==null)
			return 0;
		Queue<Node<Integer>> q = new Queue<>();
		q.enqueue(root);
		sum=sum+root.data;
		while(!q.isEmpty()){
			Node<Integer> dequeued=q.dequeue();
			for(Node<Integer> child:dequeued.children){
				sum=sum+child.data;
				q.enqueue(child);
			}
		}
		return sum;
	}
	
	public static int findNextLargest(Node<Integer> root, int value) throws Lecture15Queue.QueueEmptyException{
		if(root==null){
			return Integer.MAX_VALUE;
		}
		int node_value=Integer.MAX_VALUE;
		Queue<Node<Integer>> q = new Queue<Node<Integer>>();
		q.enqueue(root);
		if(root.data>value)
			node_value=root.data;
		while(!q.isEmpty()){
			Node<Integer> dequequed=q.dequeue();
			for(Node<Integer> child:dequequed.children){
				if(child.data>value&&child.data<node_value)
					node_value=child.data;
				q.enqueue(child);
			}
		}
		
		return node_value;
		
	}
	
	
	public static int numberOfLeaf(Node<Integer> root) throws Lecture15Queue.QueueEmptyException{
		int count=0;
		if(root==null)
			return 0;
		if(root.children.size()==0)
			return 1;
		Queue<Node<Integer>> q = new Queue<Node<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			Node<Integer> dequequed=q.dequeue();
			for(Node<Integer> child:dequequed.children){
				if(child.children.size()==0)
					count++;
				else{
					q.enqueue(child);
				}
			}
		}
		return count;
	}
	
	public static void main(String[]args) throws Lecture15Queue.QueueEmptyException{
		Node<Integer> root=levelWiseInput();
		System.out.println(numberOfLeaf(root));
		
	}
	
	
}
