package LinkedList;

import java.util.Scanner;

/*
 * 
 * Linked List Implementation 
 * 
 * 
 * */

public class LinkedListUse {
	
	/**
	 * Function to create or takeInput to a LinkedList
	 * */
	static Node<Integer> takeInput(){
		Scanner sc = new Scanner(System.in);
		int data=Integer.MIN_VALUE;
		Node<Integer> head=null;
		Node<Integer> tail=null;
		System.out.print("Enter data: ");
		data=sc.nextInt();
		while(data!=-1){
			Node<Integer> newNode = new Node(data);
			if(head==null){
				head=newNode;
				tail=newNode;
			}
			else{
				tail.next=newNode;
				tail=newNode;
			}
			System.out.print("Enter data: ");
			data=sc.nextInt();
		}
		
		return head;
		
	}
	
	/**
	 * Returns the length of the Linked List iteratively
	 * */
	static int getLength(Node<Integer> head){
		if(head==null){
			return 0;
		}
		int length=1;
		Node<Integer> temp=head;
		while(temp.next!=null){
			temp=temp.next;
			length++;
		}
		
		return length;
	}
	
	
	/**
	 * Returns the length of the Linked List recursively
	 * */
	static int getLengthRecursive(Node<Integer> head){
		if(head==null){
			return 0;
		}
		if(head.next==null){
			return 1;
		}
		
		return 1+getLength(head.next);
	}
	
	static void printLL(Node<Integer> head){
		if(head==null){
			System.out.println("Empty linked list");
			return;
		}
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
		
		System.out.println();
	}
	
	static void printElement(Node<Integer> head, int index){
		if(head==null){
			System.out.println("List is empty");
		}
		Node<Integer> temp=head;
		int i=0;
		while(temp!=null){
			if(i==index){
				System.out.println("Node found with value: "+temp.data);
				return;
			}
			temp=temp.next;
			i++;
		}
		
		if(temp==null){
			System.out.println("Node not found");
			return;
		}
	}
	
	static Node<Integer> inserAt(Node<Integer> head, int index,int value){
		if(head==null){
			System.out.println("List is empty");
			return head;
		}
		if(index==0){
			Node<Integer> newNode = new Node(value);
			newNode.next=head.next;
			head=newNode;
			return head;
		}
		
		int i=0;
		Node<Integer> temp= head;
		while(temp!=null){
			if(i==index-1){
				Node<Integer> newNode= new Node(value);
				newNode.next=temp.next;
				temp.next=newNode;
				return head;
			}
			temp=temp.next;
			i++;
		}
		if(temp==null){
			System.out.println("Invalid index");
		}
		return head;
	}
	
	
	static Node<Integer> deleteAt(Node<Integer> head, int index ){
		if(head==null){
			System.out.println("List is empty cannot delete element");
			return head;
		}
		if(index==0){
			Node<Integer> nodeToRemove=head;
			head=head.next;
			return head;
		}
		Node<Integer> temp=head;
		int i=0;
		while(temp!=null){
			if(i==index-1){
				Node<Integer> nodeToRemove=temp.next;
				temp.next=nodeToRemove.next;
				return head;
			}
		}
		if(temp==null){
			System.out.println("Invalid index");
		}
		
		return head;
	}
	public static void main(String[] args){
		Node<Integer> head=takeInput();
		printLL(head);
		head=inserAt(head, 3,9);
		System.out.println(getLengthRecursive(head));
		printElement(head,1);
		printLL(head);
	}

}
