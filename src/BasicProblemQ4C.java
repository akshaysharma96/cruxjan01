import java.util.Scanner;


public class BasicProblemQ4C {

	/**
	 * @param args
	 *
	 *Print pattern
	 *    *
	 *   ***
	 *  ******
	 * ********
	 *  ******
	 *   ***
	 *    * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		N=sc.nextInt();
		sc.nextLine();
		int rows=1;
		while(rows<=N/2+1){
			int spaces=1;
			while(spaces<=N-rows){
				System.out.print(" ");
				spaces++;
			}
			int col=1;
			while(col<=2*rows-1){
				System.out.print("*");
				col++;
			}
			rows++;
			System.out.println();
		}
		rows=N/2;
		while(rows>=1){
			int spaces=1;
			while(spaces<=N-rows){
				System.out.print(" ");
				spaces++;
			}
			int col=1;
			while(col<=2*rows-1){
				System.out.print("*");
				col++;
			}
			rows--;
			System.out.println();
		}
	}

}
