package Lecture4;

public class BinarySearch {

	/**
	 * Perform Binary Search
	 */
	public static boolean binarySearch(int a[],int element){
		int low=0;
		int high=a.length-1;
		int mid=(low+high)/2;
		while(low<=high){
			if(a[mid]==element){
				System.out.print("Element found at index: "+mid);
				return true;
			}
			else if(element<a[mid]){
				low=low;
				high=mid-1;
				mid=(low+high)/2;
				System.out.println(mid);
			}
			else{
				low=mid+1;
				high=high;
				mid=(low+high)/2;
			}
		}
		System.out.print("Element not found");
		return false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={1,2,3,4,5,6,7,8};
		int element=1;
		binarySearch(a, element);
	}

}
