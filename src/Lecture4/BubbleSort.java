package Lecture4;

public class BubbleSort {

	/**
	 * Perform Bubble Sort
	 */
	public static void bubbleSort(int a[]){
		for(int i=0;i<a.length;i++){
			for(int j=0;j<a.length-1-i;j++){
				if(a[j]>a[j+1]){
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[]={6,5,4,3,2,1};
		long startTime=System.nanoTime();
		bubbleSort(a);
		long endTime=System.nanoTime();
		System.out.println(endTime-startTime);
		for(int i=0;i<a.length;i++){
			System.out.println(a[i]);
		}
	}

}
