package Stack;

public class StackLinkedListUse {

	public static void main(String[] args) {
		Stacks<Integer> st = new Stacks<Integer>();
		for(int i=0;i<100;i++){
			st.push(i);
		}

		for(int i=0;i<99;i++){
			try {
				System.out.println(st.pop());
				System.out.println("-----");
			} catch (StackEmptyExcemption e) {
				// TODO Auto-generated catch block
				System.out.println("Stack full!");
			}
		}
	}

}
