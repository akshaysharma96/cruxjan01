package Assignment4;

public class Assignment4Q4class {

	/**
	 * Return highest occurring char in the string. 
	 */
	
	public static void highestOccurringChar(String str){
		int countAlpha[]= new int[26];
		for(int i=0;i<str.length();i++){
			countAlpha[str.charAt(i)-'a']++;
		}
		int max=Integer.MIN_VALUE;
		for(int i=0;i<countAlpha.length;i++){
			if(max<countAlpha[i]){
				max=countAlpha[i];
			}
		}
		int index=0;
		for(int i=0;i<countAlpha.length;i++){
			if(countAlpha[i]==max){
				index=i;
			}
		}
		char c=(char) ('a'+index);
		System.out.print(c);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="aabbcddddfffffffffffff";
		highestOccurringChar(str);
	}

}
