package Assignment4;

public class Assigment4Q4 {

	/**
	 * Return highest occurring char in the string. 
	 */
	public static char returnMaxChar(String str){
		int maxIter=1;
		int count=1;
		int i,j;
		int index=Integer.MIN_VALUE;
		for(i=0;i<=str.length();){
			if(count>maxIter){
				maxIter=count;
				index=i-count;
			}
			count=1;
			for(j=i+1;j<str.length()&&str.charAt(i)==str.charAt(j);j++){
				count++;
			}
			i=j;
		}
		return str.charAt(index);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="abccccdeeeeeee";
		System.out.println(returnMaxChar(str));
	}

}
