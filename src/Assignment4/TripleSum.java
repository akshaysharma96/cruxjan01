package Assignment4;

public class TripleSum {

	/**
	 *Assignment-4,Q-2
	 *
	 *Given an array of integers A and an integer x. Find triplets of elements
	 *in A which sum to x.
	 */
	public static void tripleSum(int a[],int element){
		int tripleCount=0;
		for(int i=0;i<a.length;i++){
			for(int j=i+1;j<a.length;j++){
				if(a[i]+a[j]>=element){
					continue;
				}
				else{
					int sum=a[i]+a[j];
					for(int k=j+1;k<a.length;k++){
						if(sum+a[k]==element){
							tripleCount++;
							System.out.println("One of the triplet is: "+"("+a[i]+","+a[j]+","+a[k]+")");
						}
					}
				}
			}
		}
		if(tripleCount==0){
			System.out.println("No triplets found");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[]={5,4,3,2,1,0};
		long startTime=System.nanoTime();
		tripleSum(a,8);
		long endTime=System.nanoTime();
		System.out.println(endTime-startTime);
	}

}
