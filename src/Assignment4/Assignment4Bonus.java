package Assignment4;

public class Assignment4Bonus {

	/**
	 *  Write code to do basic string compression. e.g. Given aaabbccds print out a3b2c2ds.
	 */
	public static String stringCompress(String str){
		String s="";
		int count=1;
		int i,j;
		for(i=0;i<str.length();){
			count=1;
			for(j=i+1;j<str.length()&&str.charAt(i)==str.charAt(j);j++){
				count++;
			}
			if(count>1){
				s=s+str.charAt(i)+count;
			}
			else{
				s=s+str.charAt(i);
			}
			i=j;
		}
		return s;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str=" aaabbccds";
		str=stringCompress(str);
		System.out.println(str);
	}

}
