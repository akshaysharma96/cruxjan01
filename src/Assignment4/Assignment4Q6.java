package Assignment4;

public class Assignment4Q6 {

	/**
	 * Find minimum length word in a string
	 */
	public static String minimumLengthWord(String str){
		String []strArray=str.split(" ");
		int min,index;
		index=Integer.MIN_VALUE;
		min=Integer.MAX_VALUE;
		for(int i=0;i<strArray.length;i++){
			if(strArray[i].length()<min){
				min=strArray[i].length();
				index=i;
			}
		}
		return strArray[index];
	}
	public static void main(String[] args) {
		String str="hi hiloomo aksdjas hi h";
		String minLength=minimumLengthWord(str);
		System.out.println(minLength);

	}

}
