import java.util.Scanner;


public class Pattern_1 {

	/**
	 * @param args
	 */
	/*Display pattern: 
	 * 1
	 * 2 3
	 * 4 5 6
	 * 7 8 9 10
	 * ......
	 * 
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int totalRows;
		Scanner sc=  new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		totalRows=sc.nextInt();
		int currentRow=1;
		int value=1;
		while(currentRow<=totalRows){
			int currentColumn=1;
			while(currentColumn<=currentRow){
				System.out.print(value+" ");
				currentColumn=currentColumn+1;
				value=value+1;
			}
			System.out.println();
			currentRow=currentRow+1;
		}
	}

}
