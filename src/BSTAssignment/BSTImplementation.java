package BSTAssignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

import BinaryTree.BinaryTree;
import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class BSTImplementation {

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}

	public static void pairsExist(BinaryTree<Integer> root,int data){
		
	}

	private static void preOrder(BinaryTree<Integer> root, HashMap<Integer, Integer> map){
		if(root==null)
			return;
		if(map.containsKey(root.data)){
			int value=map.get(root.data);
			value++;
			map.put(root.data, value);
		}else{
			map.put(root.data, 1);
		}
		preOrder(root.left, map);
		preOrder(root.right, map);
	}
	public static BinaryTree<Integer> insertNode(BinaryTree<Integer> root,int data){
		if(root==null){
			root=new BinaryTree<Integer>(data);
			return root;
		}
		if(data<=root.data){
			root.left=insertNode(root.left, data);
		}
		else{
			root.right=insertNode(root.right, data);
		}

		return root;
	}


	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}

	public static void search(BinaryTree<Integer> root,int data){
		if(root==null){
			System.out.println("Eithet the tree is empty or the node deos not exist");
			return;
		}
		if(root.data==data){
			System.out.println("Node found");
			return;
		}

		if(root.data<data)
			search(root.right, data);
		else
			search(root.left, data);
	}

	public static int getSize(BinaryTree<Integer> root){
		return root.size;
	}

	public static boolean isEmpty(BinaryTree<Integer> root){
		return root.size==0;
	}

	public static void insertCopy(BinaryTree<Integer> root){
		if(root==null){
			return;
		}

		BinaryTree<Integer> copy=new BinaryTree<Integer>(root.data);
		copy.left=root.left;
		root.left=copy;
		insertCopy(copy.left);
		insertCopy(root.right);
	}

	public static void printSumOfNodes(BinaryTree<Integer> root,int data){
		if(root==null)
			return;

	}

	public static Quad findMaxBST(BinaryTree<Integer> root){
		if(root==null){
			Quad q = new Quad(true,Integer.MIN_VALUE,Integer.MAX_VALUE, 0);
			return q;
		}
		Quad q1 =findMaxBST(root.left);
		Quad q2=findMaxBST(root.right);

		Quad result=null;
		if(q1.isBST&&q2.isBST){
			if(root.data>q1.max&&root.data<q2.min){
				int max=Math.max(root.data,Math.max(q1.max, q2.max));
				int min=Math.min(root.data, Math.min(q1.min, q2.min));
				result=new Quad(true,max,min,root.data+q1.sizeValue+q2.sizeValue);
			}
			else{
				int max=Math.max(q1.max, q2.max);
				int min=Math.min(q1.min,q2.min);
				result= new Quad(false, max,min,Math.max(q1.sizeValue, q2.sizeValue));
			}
		}
		else{
			if(q1.isBST){
				return q1;
			}
			else if(q2.isBST){
				return q2;
			}
			else
				return new 	Quad(false,Integer.MIN_VALUE,Integer.MAX_VALUE, 0);
		}
		return result;
	}

	public static BinaryTree<Integer> lowestCommonBST(BinaryTree<Integer> root, int data_1, int data_2){
		if(root==null)
			return root;

		if(root.data<data_1&&root.data<data_2)
			lowestCommonBST(root.right, data_1, data_2);

		if(root.data>data_1&&root.data>data_2){
			lowestCommonBST(root.left, data_1, data_2);
		}

		return root;
	}

	public static BinaryTree<Integer> lowestCommonAncestor(BinaryTree<Integer> root,int data_1,int data_2){
		if(root==null)
			return null;

		if(root.data==data_1||root.data==data_2){
			return root;
		}

		BinaryTree<Integer> leftSubtree=lowestCommonAncestor(root.left, data_1, data_2);
		BinaryTree<Integer> rightSubtree=lowestCommonAncestor(root.right, data_1, data_2);

		if(leftSubtree!=null&&rightSubtree!=null){
			return root;
		}

		return leftSubtree!=null?leftSubtree:rightSubtree;
	}
	
	public static int putGreaterSum(BinaryTree<Integer> root,int sum){
		if(root==null)
			return sum;
	
		int sumFound=putGreaterSum(root.right, sum);
		sumFound=sumFound+root.data;
		root.data=sumFound;
		sumFound=putGreaterSum(root.left, sumFound);
		
		return sumFound;
	}
	
	public static void nodeAtKDistance(BinaryTree<Integer> root,int k){
		if(root==null||k<0){
			return;
		}
		
		if(k==0){
			System.out.print(root.data+" ");
			return;
		}
		
		nodeAtKDistance(root.left, k-1);
		nodeAtKDistance(root.right, k-1);
	}
	
	public static void main(String[]args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		//		root=insertNode(root, 21);
		//		levelWisePrint(root);
		//		search(root, 21);
		//		System.out.println(getSize(root));
		//		insertCopy(root);
		//		levelWisePrint(root);
		//		Quad reQuad=findMaxBST(root);
		//		System.out.println(reQuad.sizeValue);
		//		BinaryTree<Integer> commonAncestor=lowestCommonBST(root, 90, 80);
		//		System.out.println(commonAncestor.data);
		//		BinaryTree<Integer> commonAncestor=lowestCommonAncestor(root, 31, 91);
		//		System.out.println(commonAncestor.data);
//		pairsExist(root, 20);
//		int data=putGreaterSum(root, 0);
//		levelWisePrint(root);
		nodeAtKDistance(root, 3);
	}

}
