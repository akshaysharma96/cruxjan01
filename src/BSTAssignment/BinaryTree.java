package BSTAssignment;

public class BinaryTree<K> {
	public K data;
	public BinaryTree<K> left;
	public BinaryTree<K> right;
	public int size=0;
	
	public BinaryTree(K data) {
		this.data=data;
	}
}
