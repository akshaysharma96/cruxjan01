package BSTAssignment;

import java.util.Scanner;


import Lecture15Queue.QueueEmptyException;
import Lecture17.BinaryTree;
import StackAndQueues.Queue;


/*
 * LCA in a BinaryTree
 * 
 * */
public class Q4 {

	public static BinaryTree<Integer> levelWiseInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}

		return root;
	}

	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}
	
	public static BinaryTree<Integer> LCA(BinaryTree<Integer> root,int v1, int v2){
		if(root==null)
			return null;
		
		if(root.data==v1||root.data==v2)
			return root;
		
		BinaryTree<Integer> leftTree=LCA(root.left,v1,v2);
		BinaryTree<Integer> rigthTree=LCA(root.right,v1,v2);
		
		if(leftTree!=null&&rigthTree!=null)
			return root;
		
		return leftTree!=null?leftTree:rigthTree;
	} 
	
	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=levelWiseInput();
		BinaryTree<Integer> LCA=LCA(root, 9, 32);
		System.out.println(LCA.data);
	}
}
