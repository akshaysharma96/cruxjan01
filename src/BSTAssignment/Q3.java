package BSTAssignment;

import java.util.ArrayList;
import java.util.Scanner;

import org.omg.PortableInterceptor.INACTIVE;

import Lecture15Queue.QueueEmptyException;
import Lecture17.BinaryTree;
import StackAndQueues.Queue;

public class Q3 {

	public static BinaryTree<Integer> levelWiseInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}

		return root;
	}

	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}

	public static boolean doPairExist(BinaryTree<Integer> root,int data){
		if(root==null)
			return false;

		ArrayList<Integer> inOrderArray=new ArrayList<Integer>();
		getInorderArray(root,inOrderArray);

		int startIndex=0;
		int endIndex=inOrderArray.size()-1;
		while(startIndex<endIndex){
			int value1=inOrderArray.get(startIndex);
			int value2=inOrderArray.get(endIndex);
			if(value1+value2==data)
				return true;
			if(value1+value2>data)
				endIndex--;
			else
				startIndex++;
		}

		return false;
	}
	private static void getInorderArray(BinaryTree<Integer> root,
			ArrayList<Integer> inOrderArray) {
		if(root==null){
			return;
		}
		getInorderArray(root.left, inOrderArray);
		inOrderArray.add(root.data);
		getInorderArray(root.right, inOrderArray);

	}

	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=levelWiseInput();
		boolean isThere=doPairExist(root, 30);
		System.out.println(isThere);
	}
}
