package BSTAssignment;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class SumToANumber {
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}
	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}
	
	public static void printSumPath(BinaryTree<Integer> root,int sum, int num,String str){
		if(root==null){
			if(sum==num){
				System.out.println(str);
			}
			return;
		}
		
		sum=root.data+sum;
		str=str+root.data+" ";
		if(num==sum){
			System.out.println(str);
			return;
		}
		printSumPath(root.left, sum, num, str);
		printSumPath(root.right, sum, num, str);
	}
	
	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		printSumPath(root, 0,30, "");
	}
}
