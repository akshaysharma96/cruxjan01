package BSTAssignment;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;


/**
 * Find the largest BST subtree in a BST.
 * */
public class Q6 {
	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}


	public static MaxSubtree getMaxBST(BinaryTree<Integer> root){
		if(root==null){
			MaxSubtree subtree= new MaxSubtree();
			subtree.maxSize=0;
			subtree.max=Integer.MIN_VALUE;
			subtree.min=Integer.MAX_VALUE;
			subtree.isBST=true;
			subtree.subTree=root;
			return subtree;
		}

		MaxSubtree left=getMaxBST(root.left);
		MaxSubtree right=getMaxBST(root.right);

		MaxSubtree result=new MaxSubtree();
		if(left.isBST&&right.isBST){
			if(root.data>left.max&&root.data<right.min){
				result.isBST=true;
				result.max=Math.max(root.data, Math.max(right.max, left.max));
				result.min=Math.min(root.data,Math.min(right.min,left.min));
				result.subTree=root;
				result.maxSize=root.data+left.maxSize+right.maxSize;
			}
			else{
				result.isBST=false;
				result.max=Math.max(root.data, Math.max(right.max, left.max));
				result.min=Math.min(root.data,Math.min(right.min,left.min));
				if(left.maxSize>right.maxSize)
					result.subTree=left.subTree;
				else
					result.subTree=right.subTree;
				result.maxSize=left.maxSize+right.maxSize;
			}
		}else{
			if(left.isBST){
				return left;
			}
			else if(right.isBST){
				return right;
			}

			else{
				MaxSubtree subtree= new MaxSubtree();
				subtree.maxSize=0;
				subtree.max=Integer.MIN_VALUE;
				subtree.min=Integer.MAX_VALUE;
				subtree.isBST=true;
				subtree.subTree=root;
				return subtree;
			}
		}

		return result; 
	}

	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		MaxSubtree result=getMaxBST(root);
		System.out.println("Node with max BST: "+result.subTree.data+" with max value: "+result.maxSize);


	}
}
