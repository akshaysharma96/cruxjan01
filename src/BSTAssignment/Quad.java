package BSTAssignment;

public class Quad {
	boolean isBST;
	int max;
	int min;
	int sizeValue;
	
	public Quad(boolean isBST,int max, int min, int sizeValue) {
		this.isBST=isBST;
		this.max=max;
		this.min=min;
		this.sizeValue=sizeValue;
	}
}
