import java.util.Scanner;


public class Assignment2Q6 {

	/**
	 * Convert a binary number to decimal
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int base2,base10,length,number,q;
		base10=0;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the binary number: ");
		base2=sc.nextInt();
		sc.nextLine();
		length=String.valueOf(base2).length();
		number=base2;
		while(length>0){
			q=(int) (number/(Math.pow(10,length-1)));
			number=(int) (number%(Math.pow(10,length-1)));
			base10=+base10+(int) (q*(Math.pow(2,length-1)));
			length--;
		}
		System.out.println("The decimal number is: "+base10);
	}

}
