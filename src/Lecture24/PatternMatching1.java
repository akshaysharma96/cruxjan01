package Lecture24;

public class PatternMatching1 {

	public static boolean match(String source, String target){
		boolean isnFound=false;
		int i,j;
		for(i=0;i<=source.length()-target.length();i++){
			if(source.charAt(i)==target.charAt(0)){
				int index=1;
				for(j=i+1;j<i+target.length();j++){
					if(source.charAt(j)!=target.charAt(index)){
						break;
					}
					else{
						index++;
					}
				}
				if(j-i==target.length()){
					System.out.println("Found");
					isnFound=true;
				}
			}
		}
		return isnFound;
	}
	public static void main(String[] args) {
		System.out.println(match("abcdabghef", "abgh"));
	}
}
