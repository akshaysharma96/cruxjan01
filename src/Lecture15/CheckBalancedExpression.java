package Lecture15;

public class CheckBalancedExpression {

	public boolean checkBalanced(String str){
		Stack<Character> st= new Stack<Character>();
		for(int i=0;i<str.length();i++){
			if(str.charAt(i)=='{'||str.charAt(i)=='['||str.charAt(i)=='('){
				st.push(str.charAt(i));
			}
			else if(str.charAt(i)=='}'||str.charAt(i)==']'||str.charAt(i)==')'){
				try {
					char ch=st.pop();
					if(ch=='('&&str.charAt(i)!=')')
						return false;
					else if(ch=='{'&&str.charAt(i)!='}')
						return false;
					else if(ch=='['&&str.charAt(i)!=']')
						return false;
				} catch (StackEmptyExcemption e) {
					System.out.println("Stack empty");
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[]args){
		String str="{a+[b+(c+d)]+(e+f)}";
		CheckBalancedExpression cb= new CheckBalancedExpression();
		System.out.println(cb.checkBalanced(str));
	}

}
