package LinkedListAssignment;

import java.util.Scanner;

public class SegregateOddEven {
	
	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}
	
	public static Node<Integer> segregateOddEven(Node<Integer> head){
		Node<Integer>oddHead=null,oddTail=null,evenHead=null,evenTail=null;
		Node<Integer>temp=head;
		while(temp!=null){
			if(temp.data%2==0){
				if(evenHead==null&&evenTail==null){
					evenHead=temp;
					evenTail=temp;
				}else{
					evenTail.next=temp;
					evenTail=temp;
				}
			}
			else{
				if(oddHead==null&&oddTail==null){
					oddHead=temp;
					oddTail=temp;
				}
				else{
					oddTail.next=temp;
					oddTail=temp;
				}
			}
			temp=temp.next;
		}
		Node<Integer> toReturn=null;
		if(evenHead==null&&oddHead==null){
			return null;
		}
		if(evenHead==null)
			toReturn=oddHead;
		if(oddHead==null)
			toReturn=evenHead;
		else{
			evenTail.next=oddHead;
			oddTail.next=null;
			return evenHead;
		}
		
		return toReturn;
	} 
	
	
	public static void printLL(Node<Integer> head){
		if(head==null)
			return;
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
		System.out.println("");
	}
	
	public static void main(String[]args){
		Node<Integer> head=takeInput();
		printLL(head);
		head=segregateOddEven(head);
		printLL(head);
		
	}
}
