package LinkedListAssignment;

import java.util.Scanner;

public class SwapNodes {
	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}

	public static Node<Integer> swapNodes(Node<Integer> head){
		if(head.next==null||head==null){
			return head;
		}
		Node<Integer> nextNode=head.next;
		Node<Integer> nextNext=nextNode.next;
		nextNode.next=head;
		head.next=swapNodes(nextNext);
		return nextNode;
	}

	public static void printLL(Node<Integer> head){
		if(head==null){
			return;
		}
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
		System.out.println();
	}

	public static Node<Integer> removeDuplicates(Node<Integer> head){
		if(head==null||head.next==null){
			return head;
		}
		if(head.data==head.next.data){
			Node<Integer> nextNode=head.next.next;
			head.next=nextNode;
			return removeDuplicates(nextNode);
		}

		return removeDuplicates(head.next);
	}
	public static void main(String[] args){
		Node<Integer> head=takeInput();
		//head=swapNodes(head);
		//removeDuplicates(head);
//		head=selectionSort(head);
//		System.out.println(head.next.data);
	}


}


