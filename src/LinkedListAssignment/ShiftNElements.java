package LinkedListAssignment;

import java.util.Scanner;

public class ShiftNElements {
	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}
	
	public static void printLL(Node<Integer> head){
		if(head==null)
			return;
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
		System.out.println("");
	}
	
	public static int getLength(Node<Integer> head){
		if(head==null)
			return 0;
		if(head.next==null)
			return 1;
		return 1+getLength(head.next);
	}
	/*
	 * Rotate LinkedList by k nodes
	 * 
	 * */
	public static Node<Integer> rotateList(Node<Integer> head, int k){
		if(k>=getLength(head))
			return head;
		Node<Integer> temp=head;
		int i=0;
		while(i<k){
			temp=temp.next;
			i++;
		}
		Node<Integer> secondNode=temp.next;
		temp.next=null;
		temp=secondNode;
		while(temp.next!=null){
			temp=temp.next;
		}
		
		temp.next=head;
		return secondNode;
	} 
	/*
	 * Append last N elements to front of list
	 * */
	public static Node<Integer> appendLast(Node<Integer> head, int k){
		int length=getLength(head);
		if(k>=length)
			return head;
		
		int i=0;
		Node<Integer> temp=head;
		while(i<k){
			temp=temp.next;
			i++;
		}
		Node<Integer> temp2=head;
		while(temp.next!=null){
			temp=temp.next;
			temp2=temp2.next;
		}
		Node<Integer> secondHead=temp2.next;
		temp=secondHead;
		temp2.next=null;
		while(temp.next!=null){
			temp=temp.next;
		}
		temp.next=head;
		return secondHead;
	}
	
	public static void main(String[]args){
		Node<Integer> head=takeInput();
		printLL(head);
		head=appendLast(head, 3);
		printLL(head);
	}
}
