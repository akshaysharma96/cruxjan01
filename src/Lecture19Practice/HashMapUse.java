package Lecture19Practice;

import java.util.HashMap;

public class HashMapUse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] arr1={1,2,2,3,4,5,6,7,8,4,2,3,6,1,8,4,2,4,5};
		int [] arr2={4,4,5,67,2};
//		printIntersection(arr1, arr2);
		int[] arr=returnNonDuplicates(arr1);
		for(int element:arr)
			System.out.print(element+" ");
	}
	
	public static void printIntersection(int[] arr1, int[] arr2){
		HashMap<Integer,Integer> map  = new HashMap<Integer,Integer>();
		
		for(int i=0;i<arr1.length;i++){
			if(map.containsKey(arr1[i])){
				int value=map.get(arr1[i]);
				value++;
				map.put(arr1[i], value);
			}
			else{
				map.put(arr1[i], 1);
			}
		}
		for(int i=0;i<arr2.length;i++){
			if(map.containsKey(arr2[i])&&map.get(arr2[i])>0){
				int value=map.get(arr2[i]);
				value--;
				map.put(arr2[i], value);
				System.out.print(arr2[i]+" ");
			}
		}
	}
	
	public static void removeDuplicates(int[] arr){
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for(int i=0;i<arr.length;i++){
			map.put(arr[i], 1);
		}
		
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])){
				map.remove(arr[i]);
			}
			else{
				arr[i]=0;
			}
		}
	}
	
	public static int[] returnNonDuplicates(int[] arr){
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i=0;i<arr.length;i++){
			map.put(arr[i], arr[i]);
		}
		int[] result = new int[map.size()];
		int j=0;
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])){
				result[j++]=arr[i];
				map.remove(arr[i]);
			}
		}
		
		return result;
	}
}
