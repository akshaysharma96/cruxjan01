import java.util.Scanner;


public class Assignment2Q3 {

	/**
	 * @param args
	 */
	/*
	 * Find x^n, take x and n from user
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x,n;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the number: ");
		x=sc.nextInt();
		sc.nextLine();
		System.out.print("Enter the power: ");
		n=sc.nextInt();
		int result=1;
		int count=0;
		while(count<n){
			result=result*x;
			count++;
		}
		System.out.println("The "+n+"th power of "+x+" is: "+result);
	}

}
