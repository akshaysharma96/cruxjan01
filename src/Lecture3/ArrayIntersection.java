package Lecture3;

public class ArrayIntersection {

	/**
	 * Print intersection of 2 arrays
	 */
	
	public static int[] arrayIntersection(int []array1,int [] array2){
		int result[];
		if(array1.length>=array1.length){
			result=new int[array1.length];
		}
		else{
			result=new int[array2.length];
		}
		int count=0;
		for(int i=0;i<array1.length;i++){
			for(int j=0;j<array2.length;j++){
				if(array1[i]==array2[j]&&!checkDuplicate(result, array1[i])){
					result[count]=array1[i];
					count++;
				}
			}
		}
		return result;
		
	}
	
	public static boolean checkDuplicate(int [] array, int number){
		for(int i=0;i<array.length;i++){
			if(array[i]==number){
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array1={1,2,4,5,6};
		int[] array2={3,6,1,5,5};
		int[] result = arrayIntersection(array1, array2);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}

}
