package Lecture3;

import java.util.Scanner;

public class FIndLargestElement {

	/**
	 * Print largest element in the array
	 */
	
	public static int[] takeInput(){
		Scanner sc= new Scanner(System.in);
		int size;
		System.out.print("Enter the size: ");
		size=sc.nextInt();
		int[] array= new int[size];
		for(int i=0;i<array.length;i++){
			System.out.print("Enter the "+i+"th element: ");
			array[i]=sc.nextInt();
		}
		return array;
	}

	public static int findLargest(int[] array){
		// since when length =0 we cannot say anything about the largest
		//element therefor we choose largest as Integer.MIN_VALUE=-(2^31)
		int largest=Integer.MIN_VALUE;
		for(int i=0;i<array.length;i++){
			if(array[i]>=largest){
				largest=array[i];
			}
		}
		return largest;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] array= takeInput();
		int largest=findLargest(array);
		System.out.println(largest);
	}

}
