package Lecture3;

public class SortArray01 {

	/**
	 * Sort an Array of 0s and 1s in 1 pass.
	 */
	
	public static void sortArray(int [] array){
		int nextZero=0;
		int i,temp;
		for(i=0;i<array.length;i++){
			if(array[i]==0){
				temp=array[i];
				array[i]=array[nextZero];
				array[nextZero]=temp;
				nextZero++;
			}
				
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={1,0,1,1,0,1,0,1,0,1,1};
		sortArray(array);
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}

}
