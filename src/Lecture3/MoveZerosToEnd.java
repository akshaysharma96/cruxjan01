package Lecture3;

public class MoveZerosToEnd {

	/**
	 * Move all the zeros of an array to the end.
	 */
	
	public static void moveAll(int array[]){
		int count=0;
		for(int i=0;i<array.length;i++){
			if(array[i]!=0){
				array[count]=array[i];
				count++;
			}
		}
		while(count<array.length){
			array[count]=0;
			count++;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int array[]={1,0,2,5,1,0,6,8,0,1};
		moveAll(array);
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}

}
