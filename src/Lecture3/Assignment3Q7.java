package Lecture3;

public class Assignment3Q7 {

	/*
	 * Find the duplicate number in an array of size n with numbers from 0 to n-2.
	 * Each number is present at least once. 
	 */
	
	public static void findDuplicate(int[] array){
		boolean flag=false;
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array.length;j++){
				if(i!=j&&array[i]==array[j]){
					System.out.println("The duplicate number is: "+array[i]+" "+j);
					flag=true;
				}
			}
		}
		if(!flag){
			System.out.println("There are no duplicates in the array");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] array={1,2,3,4,5,6,7};
		findDuplicate(array);
	}

}
