package Lecture3;

import java.util.Scanner;

public class SumArrayElements {

	/**
	 * @param args
	 * 
	 * Sum of all elements of an array
	 */
	
	public static int sumElements(int[]a){
		int sum=0;
		for(int i=0;i<a.length;i++){
			sum=sum+a[i];
		}
		return sum;
	}
	
	public static int[] takeInput(){
		Scanner sc= new Scanner(System.in);
		int size;
		System.out.print("Enter the size: ");
		size=sc.nextInt();
		int[] array= new int[size];
		for(int i=0;i<array.length;i++){
			System.out.print("Enter the "+i+"th element: ");
			array[i]=sc.nextInt();
		}
		return array;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={1,2,3,4,5,6};
		int [] array2= takeInput();
		int sum=sumElements(array);
		int sum2= sumElements(array2);
		System.out.println(sum);
		System.out.println(sum2);
	}

}
