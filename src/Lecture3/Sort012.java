package Lecture3;

public class Sort012 {

	/**
	 * Sort an array of 0,1,2 in one pass
	 */
	
	public static void sort(int[]array){
		int nextZero=0;
		int temp;
		int nextTwo=array.length-1;
		for(int i=0;i<array.length;i++){
			if(array[i]==0){
				temp=array[i];
				array[i]=array[nextZero];
				array[nextZero]=temp;
				nextZero++;
			}
			else if(array[i]==2){
				temp=array[i];
				array[i]=array[nextTwo];
				array[nextTwo]=temp;
				nextTwo--;
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={0,1,2,1,0,1,2,1,0,2,1,1,0};
		sort(array);
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}

}
