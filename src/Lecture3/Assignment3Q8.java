package Lecture3;

public class Assignment3Q8 {

	/**
	 * Sort an array
	 */
	public static void sortArray(int[] array){
		int key,j;
		for(int i=1;i<array.length;i++){
			j=i-1;
			key=array[i];
			while(j>=0&&key<array[j]){
				array[j+1]=array[j];
				j--;
			}
			array[j+1]=key;
		}
	}
	
	public static void printArray(int [] array){
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={8,7,6,5,4,3,2,1};
		sortArray(array);
		printArray(array);
	}

}
