package Lecture3;

import java.util.Scanner;

public class LinearSearch {

	/**
	 * Perform Linear Search
	 */
	public static boolean linearSearch(int []array, int element){
		for(int i=0;i<array.length;i++){
			if(array[i]==element){
				System.out.print("The element "+element+" exists at index: "+i);
				return true;
			}
		}
		System.out.print("The element "+element+" doesn't exist");
		return false;
	}
	
	/*
	 * Takes input element of an array
	 * */
	public static int[] takeInput(){
		Scanner sc= new Scanner(System.in);
		int size;
		System.out.print("Enter the size: ");
		size=sc.nextInt();
		int[] array= new int[size];
		for(int i=0;i<array.length;i++){
			System.out.print("Enter the "+i+"th element: ");
			array[i]=sc.nextInt();
		}
		return array;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int element;
		int array[]= takeInput();
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the element to find: ");
		element=sc.nextInt();
		boolean exists=linearSearch(array, element);
		//System.out.println(exists);
	}

}
