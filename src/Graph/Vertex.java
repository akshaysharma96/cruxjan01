package Graph;

import java.util.ArrayList;

public class Vertex {
	
	public String name;
	private ArrayList<Edge> edges;
	
	public Vertex(String name) {
		this.name=name;
		edges=new ArrayList<Edge>();
	}
	
	public int getEdgesNumber(){
		return this.edges.size();
	}

	public void addEdge(Vertex vertex_2) {
		Edge edge = new Edge(this, vertex_2);
		edges.add(edge);
	}

	public boolean isAdjacentVertex(Vertex vertex_2) {
		String name=vertex_2.name;
		for(Edge e :this.edges){
			if(e.first.name.equals(name)||e.second.name.equals(name))
				return true;
				
		}
		
		return false;
	}
	
	public ArrayList<Vertex> getAdjacentVertices(){
		ArrayList<Vertex> adjacentVertices=new ArrayList<Vertex>();
		for(Edge e:this.edges){
			if(e.first.name.equals(this.name)){
				adjacentVertices.add(e.second);
			}
			else if(e.second.name.equals(this.name)){
				adjacentVertices.equals(e.first);
			}
		}
		
		return adjacentVertices;
	}

	public void removeEdgeWith(Vertex v1) {
		// TODO Auto-generated method stub
		for(Edge e:this.edges){
			if(e.first.name.equals(v1.name)||e.second.name.equals(v1.name)){
				this.edges.remove(e);
				return;
			}
		}
	}

	public void printAdjacentVertices() {
		// TODO Auto-generated method stub
		for(Edge e :this.edges){
			if(e.first.name.equals(this.name)){
				System.out.print(e.second.name+",");
			}
			else if(e.second.name.equals(this.name)){
				System.out.print(e.first.name+",");
			}
		}
	}
}
