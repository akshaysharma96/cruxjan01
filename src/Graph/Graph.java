package Graph;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {

	String name;
	ArrayList<Vertex> vertices;

	public Graph(String name) {
		this.name=name;
		vertices=new ArrayList<Vertex>();
	}

	public boolean isEmpty(){
		return vertices.size()==0;
	}

	public int noOfVertices(){
		return vertices.size();
	}

	public int totalNumberOfEdges(){
		int count=0;
		for(Vertex vertex:this.vertices){
			count+=vertex.getEdgesNumber();
		}
		count=count/2;
		return count;
	}

	public boolean isTree(){
		int vertices=this.noOfVertices();
		int edges=this.totalNumberOfEdges();
		return (vertices-1)==edges;
	}

	public Vertex getVertex(String name){

		for(Vertex vertex:this.vertices){
			if(vertex.name.equals(name)){
				return vertex;
			}
		}

		return null;
	}

	public void addVertex(String name) throws DuplicateVertexException{
		Vertex found=getVertex(name);
		if(found!=null){
			throw new DuplicateVertexException();
		}
		Vertex vertex=new Vertex(name);
		this.vertices.add(vertex);
	}

	public void addEdge(String name_vertex_1, String name_vertex_2) throws VertexNotFoundException{
		Vertex vertex_1 = getVertex(name_vertex_1);
		Vertex vertex_2 = getVertex(name_vertex_2);
		
		if(vertex_1==null||vertex_2==null){
			throw new VertexNotFoundException();
		}
		if(vertex_1.isAdjacentVertex(vertex_2)){
			return;
		}
		vertex_1.addEdge(vertex_2);
		vertex_2.addEdge(vertex_1);
	}
	
	public void removeVertex(String name) throws VertexNotFoundException{
		Vertex v1 = getVertex(name);
		if(v1==null){
			throw new VertexNotFoundException();
		}
		for(Vertex temp:v1.getAdjacentVertices()){
			temp.removeEdgeWith(v1);
		}
		this.vertices.remove(v1);
	}
	
	public void removeEdge(String vertex_1, String vertex_2) throws VertexNotFoundException{
		Vertex v1 = getVertex(vertex_1);
		Vertex v2 = getVertex(vertex_2);
		if(v1==null||v2==null){
			throw new VertexNotFoundException();
		}
		
		v1.removeEdgeWith(v2);
		v2.removeEdgeWith(v1);
	}
	
	public void printGraph(){
		for(Vertex v1:this.vertices){
			System.out.print(v1.name+": ");
			v1.printAdjacentVertices();
			System.out.println();
		}
	}
	
	
	public static void findPath(String vertex_1, String vertex_2){
		Vertex v1 = new Vertex(vertex_1);
		Vertex v2=new Vertex(vertex_2);
		if(v1==null||v2==null)
			return;
		
		HashMap<Vertex,Boolean> map =  new HashMap<Vertex,Boolean>();
		ArrayList<String> path=new ArrayList<String>();
		
		hasPath(v1,v2,path,map);
		System.out.println(v1.getAdjacentVertices());
		
		System.out.println(path);
	}

	private static void hasPath(Vertex v1, Vertex v2, ArrayList<String> path,
			HashMap<Vertex, Boolean> map) {
		if(v1.isAdjacentVertex(v2)){
			path.add(v2.name);
			path.add(v1.name);
			return;
		}
		map.put(v1,true);
		for(Vertex v:v1.getAdjacentVertices()){
			if(!map.containsKey(v)){
				path.add(v.name);
				hasPath(v, v2, path, map);
			}
		}
		
	}

	public static void main(String[] args) throws DuplicateVertexException, VertexNotFoundException {
		Graph g1 = new Graph("My graph");
		g1.addVertex("A");
		g1.addVertex("B");
		g1.addVertex("C");
		g1.addVertex("E");
		g1.addEdge("A", "B");
		g1.addEdge("A", "C");
		g1.addEdge("A", "E");
		g1.addEdge("B", "B");
		g1.addEdge("B", "E");
		findPath("A","E");
//		g1.printGraph();
//		System.out.println(g1.totalNumberOfEdges());
//		System.out.println(g1.isTree());
	}
}
