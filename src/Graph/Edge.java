package Graph;

public class Edge {
	Vertex first;
	Vertex second;
	
	public Edge(Vertex v1, Vertex v2) {
		this.first=v1;
		this.second=v2;
	}
}
