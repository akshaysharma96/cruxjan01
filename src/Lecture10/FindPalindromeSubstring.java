package Lecture10;

public class FindPalindromeSubstring {

	/**
	 * Find the subsequences of a string, that are palindromes in O(n^2)
	 */

	public static int palindromeSubsequence(String str){
		int i,left, right;
		int count=0;
		for(i=0;i<str.length();i++){
			left=i-1;
			right=i+1;
			System.out.print(str.charAt(i)+" ");
			count++;
			while((left>=0&&right<str.length())&&str.charAt(left)==str.charAt(right)){
				System.out.print(str.substring(left,right+1)+" ");
				left--;
				right++;
				count++;
			}
		}
		i=0;
		for(i=0;i<str.length()-1;i++){
			if(str.charAt(i)==str.charAt(i+1)){
				System.out.print(str.substring(i,i+2)+" ");
				count++;
				right=i+2;
				left=i-1;
				while((left>=0&&right<str.length())&&str.charAt(left)==str.charAt(right)){
					System.out.print(str.substring(left,right+1)+" ");
					left--;
					right++;
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=palindromeSubsequence("aabaa");
		System.out.println("\n"+ count);
	}

}
