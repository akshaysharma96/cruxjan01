package HeapsAssignment;

import java.util.HashMap;

public class Q5 {

	public static void findPairs(int[]arr,int k){
		if(arr.length>1){
			HashMap<Integer, Integer> map = new HashMap<Integer,Integer>();
			for(int i=0;i<arr.length;i++){
				if(map.containsKey(arr[i])){
					map.put(arr[i],map.get(arr[i])+1);
				}else{
					map.put(arr[i],1);
				}
			}
			for(int i=0;i<arr.length;i++){
				if(map.get(arr[i])>0){
					int nextNumber=arr[i]-k;
					if(map.containsKey(nextNumber)&&map.get(nextNumber)>0){
						map.put(nextNumber, map.get(nextNumber)-1);
						System.out.println("Pair found: "+arr[i]+"-"+nextNumber+"="+k);
					}
					map.put(arr[i], map.get(arr[i])-1);
				}
			}
		}
		else{
			System.out.println("Pairs don't exist.");
		}
	}

	public static void main(String[] args) {
		int[] arr ={7,11,3,7,3,7,7};
		findPairs(arr, 4);
	}
}
