package HeapsAssignment;

import java.util.HashMap;

public class Q1 {

	public static void extractUniqueCharacters(String str){
		if(str.length()==0||str.length()==1)
			return;
		HashMap<Character, Integer> map = new HashMap<Character,Integer>();
		
		for(int i=0;i<str.length();i++){
			map.put(str.charAt(i), 1);
		}
		
		System.out.println(map);
	}
	
	public static void main(String[] args) {
		String str = "98lsdkjfowqu0e9i2ojej";
		extractUniqueCharacters(str);
	}
}
