package HeapsAssignment;

import java.util.HashMap;

public class Q3a {
	
	public static int maxFrequencyNumber(int[] arr){
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])){
				map.put(arr[i], map.get(arr[i])+1);
			}
			else{
				map.put(arr[i], 1);
			}
		}
		int number=Integer.MIN_VALUE;
		int maxValue=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			int value=map.get(arr[i]);
			if(value>=maxValue){
				maxValue=value;
				number=arr[i];
			}
		}
		
		System.out.println(number+":"+maxValue);
		return maxValue;
	}
	
	public static void main(String[] args) {
		int[] arr={1,1,3,5,6,7,7,8,9,10,11,11,11,12,12,12};
		maxFrequencyNumber(arr);
	}
}
