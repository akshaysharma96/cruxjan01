package HeapsAssignment;

import java.util.ArrayList;
import java.util.HashMap;

import Lecture20.PriorityQ;

public class Q2 {

	public static void maxConsecutiveSequence(int[] arr){
		if(arr.length==0||arr.length==1)
			return;

		PriorityQ pq = new PriorityQ();
		for(int i=0;i<arr.length;i++)
			pq.insert(arr[i]);
		ArrayList<Integer> list=new ArrayList<Integer>();
		int previousSize=1;
		list.add(pq.removeMinimum());
		while(!pq.isEmpty()){
			int element=pq.getMin();
			if(element-list.get(list.size()-1)==1){
				list.add(pq.removeMinimum());
				if(list.size()>previousSize)
					previousSize=list.size();
			}
			else{
				list=new ArrayList<Integer>();
				list.add(pq.removeMinimum());
			}
		}

		System.out.println(previousSize);

	}

	public static void main(String[] args) {
		int[]arr={2,12,9,16,10,5,3,20,25,11,1,8,6};
		maxConsecutiveSequence(arr);
	}
}
