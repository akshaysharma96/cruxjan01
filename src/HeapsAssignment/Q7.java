package HeapsAssignment;

import java.util.ArrayList;
import java.util.Arrays;

import Lecture20.PriorityQueue;

public class Q7 {
	public static void sortAlmostSorted(int[] arr,int k){
		PriorityQueue pq = new PriorityQueue();
		for(int i=0;i<k;i++){
			pq.insert(arr[i]);
		}
		int index=0;
		for(int i=k;i<arr.length;i++){
			arr[index++]=pq.removeMin();
			pq.insert(arr[i]);
		}
		
		while(!pq.isEmpty()){
			arr[index++]=pq.removeMin();
		}
	}
	
	public static void main(String[] args) {
		int arr [] = {6, 2 , 4 , 11 , 9 , 8};
		sortAlmostSorted(arr, 3);
		for(int i :arr){
			System.out.print(i+" ");
		}
	}
}
