package HeapsAssignment;

import java.util.ArrayList;
import java.util.Arrays;

import Lecture20.PriorityQueue;

public class Q6 {

	public static ArrayList<Integer> mergeKArrayList(ArrayList<ArrayList<Integer>>arr,int k){
		PriorityQueue pq = new PriorityQueue();
		for(int i=0;i<k;i++){
			ArrayList<Integer> list=arr.get(i);
			for(int j=0;j<list.size();j++){
				pq.insert(list.get(j));
			}
		}

		ArrayList<Integer> result=new ArrayList<Integer>();
		while(!pq.isEmpty()){
			result.add(pq.removeMin());
		}

		return result;
	}
	public static void main(String[] args) {
		ArrayList<Integer> l1=new ArrayList<Integer>(Arrays.asList(1, 3, 5, 7));
		ArrayList<Integer> l2= new ArrayList<Integer>(Arrays.asList(2, 4, 6, 8));
		ArrayList<Integer> l3=new ArrayList<Integer>(Arrays.asList(0, 9, 10, 11));
		ArrayList<ArrayList<Integer>>arr = new ArrayList<>();
		arr.add(l1);
		arr.add(l2);
		arr.add(l3);
		ArrayList<Integer> result =mergeKArrayList(arr, 3);
		System.out.println(result);
	}
}

