package HeapsAssignment;

import java.util.HashMap;

public class Q3b {

	public static int maxFrequencyNumber(int[] arr){
		if(arr.length==0){
			return -1;
		}
		if(arr.length==1){
			return arr[0];
		}
		
		HashMap<Integer, Integer> map = new HashMap<Integer,Integer>();
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])){
				map.put(arr[i],map.get(arr[i])+1);
			}
			else{
				map.put(arr[i], 1);
			}
		}
		
		int number=0;
		int maxFrequency=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			int frequency=map.get(arr[i]);
			if(frequency>maxFrequency){
				maxFrequency=frequency;
				number=arr[i];
			}
		}
		
		System.out.println(number+":"+maxFrequency);
		return maxFrequency;
	}
	public static void main(String[] args) {
		int[] arr={3,2,5,74,2,5,7,2,8,3,5,5,8,2,4,5,8,4,2,8,36,4};
		maxFrequencyNumber(arr);
	}
}
