package HeapsAssignment;

import Lecture20.MaxHeap;

public class Q9 {

	public static int[] kSmallestElements(int[]arr,int k){
		MaxHeap mh = new MaxHeap();
		for(int i=0;i<k;i++){
			mh.insert(arr[i]);
		}
		
		for(int i=k;i<arr.length;i++){
			if(arr[i]<mh.getMax()){
				mh.removeMax();
				mh.insert(arr[i]);
			}
		}
		int[] result=new int[k];
		int index=0;
		while(!mh.isEmpty()){
			result[index++]=mh.removeMax();
		}
		
		return result;
	}
	public static void main(String[] args) {
		int[] arr={4,2,7,3,6,9,32,12,1,8};
		int[] result=kSmallestElements(arr, 3);
		for(int i:result){
			System.out.print(i+" ");
		}
	}
}
