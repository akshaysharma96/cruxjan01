package Lecture7;

public class MergeSortedArrays {

	/**
	 * Merge two sorted arrays
	 */
	public static int[] merge(int a[],int b[]){
		int result[] = new int[a.length+b.length];
		int i=0;
		int j=0;
		int k=0;
		while(i<a.length&&j<b.length){
			if(a[i]<b[j])
				result[k++]=a[i++];
			else
				result[k++]=b[j++];
		}

		while(i<a.length){
			result[k++]=a[i++];
		}

		while(j<b.length){
			result[k++]=b[j++];
		}

		return result;
	}
	/**
	 * MergeSort
	 * @return int[] array
	 */
	public static int[] mergeSort(int a[]){
		if(a.length==1||a.length==0){
			return a;
		}
		int[] a1= new int[a.length/2];
		int[] a2=new int[a.length-(a.length/2)];
		int i;
		for(i=0;i<a.length/2;i++){
			a1[i]=a[i];
		}
		int j;
		int k;
		for(j=i,k=0;j<a.length;j++,k++){
			a2[k]=a[j];
		}
		a1=mergeSort(a1);
		a2=mergeSort(a2);
		int[] output=merge(a1, a2);

		return output;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={1,2,3,7,9,10};
		int[] b={4,8,12};
		int[] result=merge(a, b);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
		System.out.println();
		int[] r={5,34,12,65,1,45,0};
		r=mergeSort(r);
		for(int i=0;i<r.length;i++){
			System.out.print(r[i]+" ");
		}
	}

}
