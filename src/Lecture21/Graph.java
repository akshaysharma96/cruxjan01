package Lecture21;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {
	private String name;
	private ArrayList<Vertex> vertices;

	public Graph(String name) {
		this.name = name;
		vertices = new ArrayList<Vertex>();
	}

	public boolean isEmpty() {
		return vertices.size() == 0;
	}

	public int numVertices() {
		return vertices.size();
	}

	public int totalNumEdgesInGraph() {
		int count = 0;
		for(Vertex v : vertices) {
			count += v.numEdgesOfVertex();
		}
		return count / 2;
	}

	private Vertex getVertexFromName(String name) {
		for(Vertex v : vertices) {
			if(v.name.equals(name)) {
				return v;
			}
		}
		return null;
	}

	public int getDegree(String name) throws VertexNoFoundException {
		Vertex v = getVertexFromName(name);
		if(v == null) {
			VertexNoFoundException e = new VertexNoFoundException();
			throw e;
		}
		return v.numEdgesOfVertex();
	}

	public void addVertex(String name) {
		Vertex v = getVertexFromName(name);
		if(v != null) {
			//			DuplicateVertexFound e = new DuplicateVertexFound();
			//			throw e;
		}
		Vertex newVertex = new Vertex(name);
		vertices.add(newVertex);
	}

	public void addEdge(String first, String second) throws VertexNoFoundException {
		Vertex firstVertex = getVertexFromName(first);
		Vertex secondVertex = getVertexFromName(second);
		if(firstVertex == null || secondVertex == null) {
			VertexNoFoundException e = new VertexNoFoundException();
			throw e;
		}
		if(firstVertex.isAdjacent(secondVertex))
			return;

		Edge newEdge = new Edge(firstVertex, secondVertex);
		firstVertex.addEdge(newEdge);
		secondVertex.addEdge(newEdge);
	}

	public void removeVertex(String name) throws VertexNoFoundException {
		Vertex v = getVertexFromName(name);
		if(v == null) {
			VertexNoFoundException e = new VertexNoFoundException();
			throw e;
		}
		ArrayList<Vertex> adjacent=v.allAdjacentVertices();
		for(Vertex temp:adjacent){
			temp.removeEdgeWith(v);
		}
		vertices.remove(v);
	}

	public void removeEdge(String first, String second) throws VertexNoFoundException{
		Vertex firstVertex = getVertexFromName(first);
		Vertex secondVertex = getVertexFromName(second);
		if(firstVertex == null || secondVertex == null) {
			VertexNoFoundException e = new VertexNoFoundException();
			throw e;
		}

		firstVertex.removeEdgeWith(secondVertex);
		secondVertex.removeEdgeWith(firstVertex);
	}

	public boolean isConnected(){
		return false;
	}

	public boolean hasPath(String first, String  second){
		return false;
	}

	public ArrayList<String> getAdjacentVertices(String name) throws VertexNoFoundException{
		Vertex vertex = getVertexFromName(name);
		if(vertex==null){
			throw new VertexNoFoundException();
		}
		ArrayList<String> result= new ArrayList<>();
		for(Vertex v:vertex.allAdjacentVertices()){
			result.add(v.name);
		}

		return result;
	}

	public void printGraph(){
		System.out.println(name);
		for(Vertex v :vertices){
			v.printAdjacentVerties();
			System.out.println(v.name);
		}

	}

	public boolean hashPath(String first, String second){ //BFS
		Vertex v1 = getVertexFromName(first);
		Vertex v2 = getVertexFromName(second);
		if(v1==null||v2==null)
			return false;
		ArrayList<Vertex> adjacent=v1.allAdjacentVertices();
		for(Vertex v:adjacent){
			if(v.name.equals(second)){
				return true;
			}
		}
		HashMap<Vertex,Boolean> map = new HashMap<Vertex, Boolean>();
		map.put(v1, true);
		for(Vertex v:adjacent){
			if(!map.containsKey(v)){
				if(hasPathHelper(v,v2,map)){
					return true;
				}
			}

		}
		return false;

	}

	private boolean hasPathHelper(Vertex v1, Vertex v2,
			HashMap<Vertex, Boolean> map) {
		// TODO Auto-generated method stub
		map.put(v1, true);
		for(Vertex vertex:v1.allAdjacentVertices()){
			if(vertex.name.equals(v2.name)){
				return true;
			}
		}

		return false;
	}






















}
