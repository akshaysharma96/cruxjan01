package Lecture21;

import java.util.ArrayList;

public class Vertex {
	String name;
	ArrayList<Edge> edgeList;

	public Vertex(String name) {
		this.name = name;
		edgeList = new ArrayList<Edge>();
	}

	public ArrayList<Vertex> allAdjacentVertices() {
		// TODO Auto-generated method stub
		ArrayList<Vertex> output=new ArrayList<Vertex>();
		for(Edge e:this.edgeList){
			if(e.first.name.equals(this.name)){
				output.add(e.second);
			}
			else if(e.second.name.equals(this.name)){
				output.add(e.first);
			}
		}
		
		return output;
	}

	public void removeEdgeWith(Vertex v) {
		// TODO Auto-generated method stub
		//		Edge newEdge=null;
		for(Edge edge:this.edgeList){
			if(edge.second.equals(v.name)||edge.first.equals(v.name)){
				this.edgeList.remove(edge);
				return;
			}
		}
	}

	public int numEdgesOfVertex() {
		// TODO Auto-generated method stub
		return this.edgeList.size();
	}

	public void addEdge(Edge newEdge) {
		this.edgeList.add(newEdge);

	}

	public boolean isAdjacent(Vertex secondVertex) {
		for(Edge e:this.edgeList){
			if(e.first.name.equals(secondVertex.name)||e.second.name.equals(secondVertex.name))
				return true;
		}
		
		return false;
	}

	public void printAdjacentVerties() {
		ArrayList<Vertex> adjacentVertices=allAdjacentVertices();
		for(Vertex adjacentVertex:adjacentVertices){
			System.out.print(adjacentVertex.name+", ");
		}
	}
	
	
}
