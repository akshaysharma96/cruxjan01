package queuepractice;

import L17.QueueEmptyException;
import LInkedList.Node;

public class QueueUsingLinkedList<T> {
	
	Node<T> front;
	Node<T> rear;
	int size=0;
	
	public void enqueue(T data){
		size++;
		if(front==null){
			Node<T> newNode = new Node<T>(data);
			front=newNode;
			rear=newNode;
			return;
		}
		Node<T> newNode = new Node<T>(data);
		rear.next=newNode;
		rear=newNode;
	}
	
	public T dequeue() throws QueueEmptyException{
		if(front==null){
			throw new QueueEmptyException();
		}
		T data=front.data;
		front=front.next;
		size--;
		return data;
	}
	
	public boolean isEmpty(){
		return front==null;
	}
	
	public int size(){
		return size;
	}
	
	public T top() throws QueueEmptyException{
		if(front==null){
			throw new QueueEmptyException();
		}
		
		return front.data;
	}
	
	public static void main(String [] args){
		QueueUsingLinkedList<Integer> ql = new QueueUsingLinkedList<Integer>();
		for(int i=0;i<8;i++){
			ql.enqueue(i+6);
		}
//		try {
////			System.out.println(ql.top());
//		} catch (QueueEmptyException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		for(int i=0;i<10;i++){
			try {
				System.out.println(ql.dequeue());
			} catch (QueueEmptyException e) {
				// TODO Auto-generated catch block
				System.out.println("Queue is empty");
			}
		}
	}
}
