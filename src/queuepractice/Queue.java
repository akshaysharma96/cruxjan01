package queuepractice;

import L17.QueueEmptyException;

public class Queue {
	
	private int[] queue;
	private int size=0;
	private int front=-1;
	private int rear=0;
	int max;
	
	public Queue(int length) {
		queue=new int[length];
		max=length;
	}
	
	public void enqueue(int data) throws QueueFullException{
		if(front==(rear+1)%max){
			throw new QueueFullException();
		}
		if(front==-1)
			front=0;
		
		queue[rear]=data;
		rear=(rear+1)%max;
		size++;
	}
	
	public boolean isEmpty(){
		return (front==rear||front==-1);
	}
	
	public int dequeue() throws QueueEmptyException{
		if(isEmpty()){
			throw new QueueEmptyException();
		}
		int data=queue[front];
		front++;
		return data;
	}
	
	
	public int top() throws QueueEmptyException{
		if(isEmpty()){
			throw new QueueEmptyException();
		}
		return queue[front];
	}
	
	public static void main(String []args){
		Queue q =new Queue(10);
		for(int i=0;i<8;i++){
			try {
				q.enqueue(i);
			} catch (QueueFullException e) {
				// TODO Auto-generated catch block
				System.out.println("Queue empty");
			}
		}
		
		for(int i=0;i<5;i++){
			try {
				System.out.println(q.dequeue());
			} catch (QueueEmptyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
