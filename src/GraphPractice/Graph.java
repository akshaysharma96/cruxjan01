package GraphPractice;

import java.util.ArrayList;
import java.util.HashMap;

import Graph.VertexNotFoundException;
import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;
import StackAndQueues.Stack;
import StackAndQueues.StackEmptyException;

public class Graph {
	String name;
	ArrayList<Vertex> vertices;


	public Graph() {
		vertices =  new ArrayList<Vertex>();
	}

	public Vertex getVertex(String name){
		for(Vertex v:this.vertices){
			if(v.name.equals(name))
				return v;
		}

		return null;
	}


	public void addVertex(String name){
		Vertex v1 = getVertex(name);
		if(v1!=null){
			System.out.println("Vertex already exists");
			return;
		}

		Vertex vertex = new Vertex(name);
		this.vertices.add(vertex);
	}

	public void addEdge(String name_vertex_1, String name_vertex_2) throws VertexNotFoundException{
		Vertex vertex_1 = getVertex(name_vertex_1);
		Vertex vertex_2 = getVertex(name_vertex_2);

		if(vertex_1==null||vertex_2==null){
			throw new VertexNotFoundException();
		}
		if(vertex_1.isAdjacentVertex(vertex_2)){
			return;
		}
		vertex_1.addEdge(vertex_2);
		vertex_2.addEdge(vertex_1);
	}


	public void printGraph(){
		for(Vertex v: this.vertices){
			System.out.print(v.name+": ");
			v.printAdjacentVertices();
			System.out.println("");
		}

	}

	public int numberOfVertices(){
		return this.vertices.size();
	}

	public int numberOfEdges(){
		int count=0;
		for(Vertex v:this.vertices){
			count+=v.degree();
		}

		return count/2;
	}

	public boolean isTree(){
		int edges=numberOfEdges();
		int vertices=numberOfVertices();
		return (vertices-1)==edges;
	}

	public void removeVertex(String first){
		Vertex v1=getVertex(first);
		if(v1==null)
			return;

		ArrayList<Vertex> adjacentVertices=v1.returnAdjacentVertices();
		for(Vertex temp:adjacentVertices){
			temp.removeEdgeWith(v1);
		}

		vertices.remove(v1);
	}

	public void removeEdge(String v1, String v2){
		Vertex vertex_1=getVertex(v1);
		Vertex vertex_2=getVertex(v2);
		if(vertex_1==null||vertex_2==null){
			return;
		}
		vertex_1.removeEdgeWith(vertex_2);
		vertex_2.removeEdgeWith(vertex_1);
	}

	public void BFS() throws QueueEmptyException{
		HashMap<Vertex, Boolean> map = new HashMap<Vertex,Boolean>();
		ArrayList<Vertex> list=vertices;
		Queue<Vertex> q = new Queue<Vertex>();
		q.enqueue(list.get(0));
		while(!q.isEmpty()){
			Vertex v1=q.dequeue();
			if(!map.containsKey(v1)){
				System.out.print(v1.name+": ");
				map.put(v1, true);
				for(Vertex v:v1.returnAdjacentVertices()){
					if(!map.containsKey(v)){
						System.out.print(v.name+", ");
						q.enqueue(v);
					}
				}
				System.out.println();
			}
		}
	}

	public void DFS() throws StackEmptyException{
		Stack<Vertex> st=new Stack<Vertex>();
		HashMap<Vertex, Boolean> map = new HashMap<Vertex,Boolean>();
		st.push(vertices.get(0));
		while(!st.isEmpty()){
			Vertex v1=st.pop();
			if(!map.containsKey(v1)){
				System.out.print(v1.name+": ");
				map.put(v1, true);
				for(Vertex v:v1.returnAdjacentVertices()){
					if(!map.containsKey(v)){
						System.out.print(v.name+", ");
						st.push(v);
					}
				}
				System.out.println("");
			}
		}
	}

	public void DFSRecursion(Vertex v1, HashMap<Vertex, Boolean> map){
		if(map.containsKey(v1)){
			return;
		}
		System.out.println(v1.name+": ");
		map.put(v1,true);
		for(Vertex v :v1.returnAdjacentVertices()){
			if(!map.containsKey(v)){
//				System.out.print(v.name+", ");
				DFSRecursion(v, map);
			}
		}
	}

	public void hasPath(String v1, String v2){
		Vertex vertex_1=getVertex(v1);
		Vertex vertex_2=getVertex(v2);
		if(v1==null||v2==null)
			return;
		HashMap<Vertex,Boolean> map = new HashMap<>();
		ArrayList<String> path=getPath(vertex_1,vertex_2,map);
	}
	private ArrayList<String> getPath(Vertex vertex_1, Vertex vertex_2,
			HashMap<Vertex, Boolean> map) {
		// TODO Auto-generated method stub
		if(vertex_1.isAdjacentVertex(vertex_2)){
			ArrayList<String> output=new ArrayList<String>();
			output.add(vertex_2.name);
			output.add(vertex_1.name);
			map.put(vertex_1,true);
			map.put(vertex_2, true);
			return output;
		}

		return null;
	}

	public static void main(String[] args) throws VertexNotFoundException, QueueEmptyException, StackEmptyException {
		Graph graph= new Graph();
		graph.addVertex("A");
		graph.addVertex("B");
		graph.addVertex("C");
		graph.addVertex("D");
		graph.addVertex("E");
		graph.addEdge("A","B");
		graph.addEdge("A","C");
		graph.addEdge("A","D");
		graph.addEdge("A","A");
		graph.addEdge("B","D");
		graph.addEdge("C","E");
		graph.addEdge("D","E");
		Vertex v1 = graph.getVertex("A");
		HashMap<Vertex, Boolean> map = new HashMap<>();
		graph.DFSRecursion(v1, map);
		System.out.println();
		System.out.println();
		graph.BFS();
	}
}
