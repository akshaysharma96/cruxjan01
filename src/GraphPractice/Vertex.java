package GraphPractice;

import java.util.ArrayList;
public class Vertex {
	
	String name;
	ArrayList<Edge> edges;
	
	public Vertex(String name) {
		this.name=name;
		edges=new ArrayList<>();
	}

	public void addEdge(Vertex v2) {
		// TODO Auto-generated method stub
		Edge edge= new Edge(this,v2);
		this.edges.add(edge);
	}
	
	public ArrayList<String> getAdjacentVertices(){
		ArrayList<String> result=new ArrayList<String>();
		for(Edge e:this.edges){
			if(e.firstVertex.name.equals(this.name)){
				result.add(e.secondVertex.name);
			}else if(e.secondVertex.name.equals(this.name)){
				result.add(e.firstVertex.name);
			}
		}
		return result;
	}
	
	public ArrayList<Vertex> returnAdjacentVertices(){
		ArrayList<Vertex> result=new ArrayList<Vertex>();
		for(Edge e:this.edges){
			if(e.firstVertex.name.equals(this.name)){
				result.add(e.secondVertex);
			}else if(e.secondVertex.name.equals(this.name)){
				result.add(e.firstVertex);
			}
		}
		return result;
	}
	
	public void printAdjacentVertices(){
		for(Edge e :this.edges){
			if(e.firstVertex.name.equals(this.name)){
				System.out.print(e.secondVertex.name+",");
			}
			else if(e.secondVertex.name.equals(this.name)){
				System.out.print(e.firstVertex.name+", ");
			}
		}
	}
	
	public int degree(){
		return this.edges.size();
	}

	public boolean isAdjacentVertex(Vertex vertex_2) {
		String name=vertex_2.name;
		for(Edge e :this.edges){
			if(e.firstVertex.name.equals(name)||e.secondVertex.name.equals(name))
				return true;
				
		}
		
		return false;
	}

	public void removeEdgeWith(Vertex v1) {
		for(Edge e:this.edges){
			if(e.firstVertex.name.equals(v1.name)||e.secondVertex.name.equals(v1.name)){
				edges.remove(e);
				return;
			}
		}
	}
}
