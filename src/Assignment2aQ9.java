import java.util.Scanner;


public class Assignment2aQ9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N,number;
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number: ");
		number=sc.nextInt();
		sc.nextLine();
		System.out.print("Enter the precision: ");
		N=sc.nextInt();
		sc.nextLine();
		double diff=1/Math.pow(10, N);
		double root=0;
		while(number-(root*root)>=diff){
			root=root+diff;
		}
		System.out.println(root);
	}

}
