import java.util.Scanner;


public class PrintPrime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter number: ");
		N=sc.nextInt();
		int number=2;
		while(number<=N){
			int count=2;
			while(count<=number/2){
				if(number%count==0){
					break;
				}
				count++;
			}
			if(count>number/2){
				System.out.println(number+" is Prime");
			}
			number++;
		}
		
	}

}
