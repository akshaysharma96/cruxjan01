package Trie;

import java.util.HashMap;

public class Node {
	
	char value;
	HashMap<Character, Node> children;
	boolean isTerminal;
	
	
	public Node(char value) {
		// TODO Auto-generated constructor stub
		this.value=value;
		children=new HashMap<Character,Node>();
		isTerminal=false;
	}
}
