import java.util.Scanner;


public class Assingment1Pattern3 {

	/**
	 * @param args
	 */
	/*
	 * Display pattern like:
	 * 1
	 * 11
	 * 121
	 * 1331
	 * 14641
	 * .......
	 * 
	 * Hint:  All are powers of 11
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		System.out.print("Enter the number of rows: ");
		Scanner sc= new Scanner(System.in);
		N=sc.nextInt();
		int rows=1;
		while(rows<=N){
			int number=(int) (Math.pow(11,rows-1));
			System.out.print(number);
			rows++;
			System.out.println();
		}
	}

}
