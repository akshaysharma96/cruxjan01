package BTAssignmentAgain;

public class BinaryTree<T> {
	public T data;
	public BinaryTree<T> left;
	public BinaryTree<T> right;
	public int size;
	
	public BinaryTree(T data) {
		this.data=data;
	}
	
	
}
