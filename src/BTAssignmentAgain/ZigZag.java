package BTAssignmentAgain;

import java.util.Scanner;

import org.omg.PortableInterceptor.INACTIVE;

import Lecture15Queue.QueueEmptyException;
import Lecture17.BinaryTree;
import StackAndQueues.Queue;
import StackAndQueues.Stack;
import StackAndQueues.StackEmptyException;

public class ZigZag {

	public static BinaryTree<Integer> levelWiseInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}

		return root;
	}

	public static void zigzag(BinaryTree<Integer> root) throws StackEmptyException{
		Stack<BinaryTree<Integer>> s1=new Stack<>();
		Stack<BinaryTree<Integer>> s2 =new Stack<>();

		s1.push(root);
		boolean first=true;
		while(true){
			if(s1.isEmpty()&&s2.isEmpty())
				return;
			if(first){
				while(!s1.isEmpty()){
					BinaryTree<Integer> popped=s1.pop();
					System.out.print(popped.data+" ");
					if(popped.left!=null)
						s2.push(popped.left);
					if(popped.right!=null)
						s2.push(popped.right);
				}
				first=false;
			}
			else{
				while(!s2.isEmpty()){
					BinaryTree<Integer> popped=s2.pop();
					System.out.print(popped.data+" ");
					if(popped.right!=null)
						s1.push(popped.right);
					if(popped.left!=null)
						s1.push(popped.left);
				}
				first=true;
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) throws QueueEmptyException, StackEmptyException {
		BinaryTree<Integer> root=levelWiseInput();
		zigzag(root);
	}
}
