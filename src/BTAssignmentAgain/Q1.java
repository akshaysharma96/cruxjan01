package BTAssignmentAgain;

import java.util.Scanner;

import BinaryTree.BinaryTree;
import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class Q1 {

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}
	
	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		System.out.println(getSum(root,0));
	}
	
	public static int getSum(BinaryTree<Integer> root,int sum){
		if(root==null){
			sum=sum+0;
			return sum;
		}
		sum=getSum(root.left,sum);
		sum=sum+root.data;
		return getSum(root.right, sum);
	}
}
