package BTAssignmentAgain;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class LevelWise {

	public static BinaryTree<Integer> levelWiseInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}

		return root;
	}
	
	public static void levelWise(BinaryTree<Integer> root) throws QueueEmptyException{
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		int level=0;
		while(!q.isEmpty()){
			for(int i=0;i<Math.pow(2, level);i++){
				if(q.isEmpty())
					return;
				BinaryTree<Integer> dequeued=q.dequeue();
				if(dequeued.left!=null)
					q.enqueue(dequeued.left);
				if(dequeued.right!=null)
					q.enqueue(dequeued.right);
				
				System.out.print(dequeued.data+" ");
				
			}
			level++;
			System.out.println("");
		}
	}
	
	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=levelWiseInput();
		levelWise(root);
	}
}
