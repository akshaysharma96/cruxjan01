package BTAssignmentAgain;

import java.util.Scanner;
import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class isBalanced {

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		root.size++;
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				root.size++;
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				root.size++;
				q.enqueue(rightTree);
			}
		}
		return root;
	}
	
	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		System.out.println(isBalanced(root).isBalanced);
	}
	
	public static Pair isBalanced(BinaryTree<Integer> root){
		if(root==null){
			Pair p = new Pair();
			p.height=0;
			p.isBalanced=true;
			return p;
		}
		
		Pair p1=isBalanced(root.left);
		Pair p2=isBalanced(root.right);
		
		Pair result=new Pair();
		result.height=Math.max(p1.height, p2.height)+1;
		if(Math.abs(p1.height-p2.height)<=1){
			result.isBalanced=true;
		}
		else{
			result.isBalanced=false;
		}
		
		return result;
	}
}
