package SegmentTress;

public class SegTree {
	int[] segtree;
	int[] arr;
	public SegTree(int[] arr) {
		segtree=new int[2*arr.length-1];
		this.arr=arr;
	}

	public void build(int node, int start, int end){
		if(start==end){
			segtree[node]=arr[start];
		}
		else{
			int mid=(start+end)/2;
			build(2*node+1, start, mid);
			build(2*node+2, mid+1, end);
			segtree[node]=segtree[2*node+1]+segtree[2*node+2];
		}
	}

	public void printSegtree(){
		for(int sum:segtree)
			System.out.print(sum+" ");
		System.out.println();
	}

	public static void main(String[] args) {
		int[] arr={1,2,3,4};
		SegTree tree= new SegTree(arr);
		tree.build(0, 0, arr.length-1);
		//		tree.printSegtree();
		tree.update(0, 0, arr.length-1, 2,6);
		//		tree.printSegtree();
	}

	public void update(int node, int start, int end, int index,int value){
		if(start==end){
			arr[index]=value;
			segtree[node]=value;
		}
		else{
			int mid=(start+end)/2;
			if(start<=index&&index<=mid){
				update(2*node+1, start, mid, index, value);
			}
			else if(index<=end&&index>mid){
				update(2*node+2,mid+1, end, index, value);
			}
			segtree[node]=segtree[2*node+1]+segtree[2*node+2];
		}
	}

//	public int query(int node,int start,int end,int l,int r){
//		if(l<start||r>end){
//			System.out.println("Out of range "+l+" "+r);
//			return 0;
//		}
//		if(l==start&&r==end)                                      
//			return segtree[node];
//		else{
//			int p1=1,p2=1;
//			int mid=(start+end)/2;
//			if(l>mid&&r<=end){
//				System.out.println("before");
//				return query(2*node+2, mid+1, end, l, r);
//			}
//			else if(l>=start&&r<=mid){
//				System.out.println("after");
//				return query(2*node+1, start, mid, l, r);
//			}
//			else if(l>=start&&r>mid){
//				System.out.println("both "+l+" "+r);
//				p1=query(2*node+1, start,mid, l, r);
//				p2=query(2*node+2, mid+1, end, l, r);
//			}
//			
//			return p1+p2;
//		}
		
//	}

}
