package SegmentTress;

public class SegmentTree {
	int[] segtree;
	int[] arr_2;
	int[] lazy;
	int N;

	SegmentTree(int[] arr){
		N=arr.length;
		segtree=new int[2*N-1];
		arr_2=arr;
		lazy=new int[2*N-1];
	}

	public void build(int start,int end, int node){
		if(start==end){
			segtree[node]=arr_2[start];
			return;
		}
		else{
			int mid=(start+end)/2;
			build(start,mid,2*node+1);
			build(mid+1,end,2*node+2);
			segtree[node]=segtree[2*node+1]+segtree[2*node+2];
		}
	}

	public void showTree(){
		for(int i:this.segtree){
			System.out.print(i+" ");
		}
	}

	public void update(int start,int end,int index,int element,int node){
		if(start==end&&start==index){
			arr_2[index]=element;
			segtree[node]=element;
			return;
		}
		else{
			int mid=(start+end)/2;
			if(index>=start&&index<=end){
				update(start,mid+1, index, element, 2*node+1);
			}
			else if(index>=mid&&index<=end){
				update(mid+1,end,index,element,2*node+2);
			}
			segtree[node]=segtree[2*node+1]+segtree[2*node+2];
		}

	}
	
	public void updateInterval(int start,int end,int l,int r,int element,int node){
		if(start>end||start>r||l>end){
			return;
		}
		
		if(start==end){
			segtree[node]=element;
			return;
		}
		else{
			int mid=(start+end)/2;
			updateInterval(start, mid, l, r, element, 2*node+1);
			updateInterval(mid+1, end, l, r, element, 2*node+2);
			segtree[node]=segtree[2*node+1]+segtree[2*node+2];
		}
	}
	

	public int getInterval(int start,int end,int b,int e,int node){
		if(b<start||e>end){
			return 0;
		}
		if(b>=start&&e<=end){
			return segtree[node];
		}

		int mid=(start+end)/2;
		int p1=getInterval(start, mid, b, e, 2*node+1);
		int p2=getInterval(mid+1, end, b, e, 2*node+2);
		return p1+p2;
	}

	public static void main(String[] args) {
		int[] arr={1,2,3,56,12,64,78};
		SegmentTree st = new SegmentTree(arr);
		st.build(0, arr.length-1, 0);
		//		st.showTree();
////		System.out.print(st.getInterval(0, arr.length-1, 5,13,0));
//		st.lazyUpdate(0, arr.length-1, 0, 1, 0,14);
		st.showTree();
	}
	
//	public void lazyUpdate(int start,int end, int l,int r, int node,int element){
//		
//		if(lazy[node]!=0){
//			segtree[node]=element;
//			if(start!=end){
//				lazy[2*node+1]=element;
//				lazy[2*node+2]=element;
//			}
//			lazy[node]=0;
//			return;
//		}
//		
//		if(start>end||start>r||l>end){
//			return;
//		}
//		
//		if(start>=l&&r<=end){
//			if(lazy[node]!=0){
//				segtree[node]=element;
//				if(start!=end){
//					lazy[2*node+1]=element;
//					lazy[2*node+2]=element;
//				}
//				lazy[node]=0;
//				return;
//			}
//		}
//		
//		int mid=(start+end)/2;
//		lazyUpdate(start, mid, l, r, 2*node+1, element);
//		lazyUpdate(mid+1, end, l, r, 2*node+2, element);
//		segtree[node]=segtree[2*node+1]+segtree[2*node+2];
//	}
//	
	
//	public int queryRange(int start,int end,int l,int r, int node, int element){
//		if(start > end || start > r || end < l)
//	        return 0;  
//		if(lazy[node])
//	}
}
