package Lecture20;

import Practice.sort01;

/*
 * Given an almost sorted array, sort the array in O(n)
 * */

public class AlmostSortedPractice {
	
	public static void sort(int[] arr,int k){
		if(arr.length==0||arr.length==1)
			return;
		PriorityQ pq = new PriorityQ();
		for(int i=0;i<k;i++){
			pq.insert(arr[i]);
		}
		
		int index=0;
		for(int i=k;i<arr.length;i++){
			arr[index++]=pq.removeMinimum();
			pq.insert(arr[i]);
		}
		
		while(!pq.isEmpty()){
			arr[index++]=pq.removeMinimum();
		}
	}
	
	public static void main(String[] args) {
		int arr[]={3,1,2,6,5,4,7,8,9};
		sort(arr,3);
		for(int i :arr){
			System.out.print(i+" ");
		}
	}
}
