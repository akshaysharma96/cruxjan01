package Lecture20;

public class AlmostSortedArray {
	
	
	public static void sortAlmostSorted(int[]arr,int k){
		if(arr.length==0){
			return;
		}
		PriorityQueue pq = new PriorityQueue();
		int  index=0;
		//Put first k elements into the array
		for(int i=0;i<k;i++){
			pq.insert(arr[i]);
		}
		//Now keep adding the remaining elements but removing minimum at each step
		for(int i=k;i<arr.length;i++){
			int min=pq.removeMin();
			arr[index++]=min;
			pq.insert(arr[i]);
		}
		
		//adding the remaining heap elements to array.
		while(!pq.isEmpty()){
			arr[index++]=pq.removeMin();
		}
	}
	
	public static int[] kLargest(int[]arr, int k){
		PriorityQueue pq = new PriorityQueue();
		for(int i=0;i<k;i++){
			pq.insert(arr[i]);
		}
		for(int i=k;i<arr.length;i++){
			if(arr[i]>pq.min()){
				pq.removeMin();
				pq.insert(arr[i]);
			}
		}
		
		int[] result= new int[k];
		int j=0;
		while(!pq.isEmpty()){
			int min = pq.removeMin();
			result[j]=min;
			j++;
		}
		
		return result;
	}
	
	public static void main(String[] args) {
//		int a [] = {2,1,3,5,4,8,7};
//		sortAlmostSorted(a, 2);
//		for(int i :a){
//			System.out.println(i);
//		}
		int a[] = {10,4,3,5,15,7,9,2,20};
		int[] result=kLargest(a, 4);
		for(int i :result ){
			System.out.print(i+" ");
		}
	}
}
