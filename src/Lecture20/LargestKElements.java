package Lecture20;

public class LargestKElements {
	
	public static int[] largestKElements(int[]arr, int k){
		PriorityQ pq =  new PriorityQ();
		
		for(int i=0;i<k;i++){
			pq.insert(arr[i]);
		}
		
		for(int i=k;i<arr.length;i++){
			if(arr[i]>pq.getMin()){
				pq.removeMinimum();
				pq.insert(arr[i]);
			}
		}
		int[]result=new int[k];
		int i=0;
		while(!pq.isEmpty()){
			result[i++]=pq.removeMinimum();
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr={3,1,2,6,5,4,7,8,9};
		int[] result=largestKElements(arr,4);
		for(int i:result)
			System.out.print(i+" ");
	}
}