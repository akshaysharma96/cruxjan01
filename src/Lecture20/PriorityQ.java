package Lecture20;

import java.util.ArrayList;

import StackAndQueues.Queue;

public class PriorityQ {
	
	ArrayList<Integer> heap;
	
	
	public PriorityQ() {
		heap=new ArrayList<Integer>();
		heap.add(null);
	}
	
	public int getSize(){
		return heap.size()-1;
	}
	
	public boolean isEmpty(){
		return getSize()==0;
	}
	
	public  int getMin(){
		if(heap.isEmpty()){
			return -1;
		}
		return heap.get(1);
	}
	
	public void insert(int data){
		heap.add(data);
		int childIndex=heap.size()-1;
		int parentIndex=childIndex/2;
		while(childIndex>1){
			int parent=heap.get(parentIndex);
			int child=heap.get(childIndex);
			
			if(child>parent){
				return;
			}
			int temp=child;
			heap.set(childIndex, parent);
			heap.set(parentIndex, temp);
			
			childIndex=parentIndex;
			parentIndex=childIndex/2;
		}
	}
	
	public int removeMinimum(){
		if(heap.isEmpty()){
			return Integer.MIN_VALUE;
		}
		
		int min=heap.get(1);
		int last=heap.get(heap.size()-1);
		heap.set(1,last);
		heap.remove(heap.size()-1);
		
		int parentIndex=1;
		int leftChildIndex=2*parentIndex;
		int rightChildIndex=leftChildIndex+1;
		
		while(parentIndex<=heap.size()-1){
			int minIndex=parentIndex;
			int minValue=heap.get(minIndex);
			
			if(leftChildIndex<=heap.size()-1){
				if(heap.get(leftChildIndex)<minValue){
					minIndex=leftChildIndex;
					minValue=heap.get(leftChildIndex);
				}
			}
			
			if(rightChildIndex<=heap.size()-1){
				if(heap.get(rightChildIndex)<minValue){
					minIndex=rightChildIndex;
					minValue=heap.get(rightChildIndex);
				}
			}
			
			if(minIndex==parentIndex)
				break;
			
			else{
				int temp=heap.get(parentIndex);
				heap.set(parentIndex,minValue);
				heap.set(minIndex,temp);
			}
			
			parentIndex=minIndex;
			leftChildIndex=2*parentIndex;
			rightChildIndex=leftChildIndex+1;
		}
		
		return min;
	}
	
	public static void main(String[] args) {
		PriorityQ pq = new PriorityQ();
		pq.insert(5);
		pq.insert(56);
		System.out.println(pq.removeMinimum());
		
	}
}
