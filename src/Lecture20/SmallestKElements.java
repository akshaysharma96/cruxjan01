package Lecture20;

public class SmallestKElements {
	
	public static int[] smallestKElements(int[] arr,int k){
		MaxHeap mh = new MaxHeap();
		
		for(int i=0;i<k;i++){
			mh.insert(arr[i]);
		}
		
		for(int i=k;i<arr.length;i++){
			if(arr[i]<mh.getMax()){
				mh.removeMax();
				mh.insert(arr[i]);
			}
		}
		
		int[] result=new int[k];
		int index=0;
		while(!mh.isEmpty()){
			result[index++]=mh.removeMax();
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr={3,1,2,6,5,4,7,8,9};
		int[] result=smallestKElements(arr, 4);
		for(int i=result.length-1;i>=0;i--){
			System.out.print(result[i]+" ");
		}
	}
}
