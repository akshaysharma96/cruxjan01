package Lecture20;

import java.util.ArrayList;

public class MaxHeap {

	ArrayList<Integer> heap;

	public MaxHeap() {
		heap=new ArrayList<Integer>();
		heap.add(null);
	}

	public void insert(int data){
		heap.add(data);
		int childIndex=heap.size()-1;
		int parentIndex=childIndex/2;

		while(childIndex>1){
			int child=heap.get(childIndex);
			int parent=heap.get(parentIndex);

			if(parent>child)
				return;

			int temp=child;
			heap.set(childIndex,parent);
			heap.set(parentIndex, temp);

			childIndex=parentIndex;
			parentIndex=childIndex/2;
		}

	}

	public int getSize(){
		return heap.size()-1;
	}

	public boolean isEmpty(){
		return getSize()==0;
	}
	public int getMax(){
		if(isEmpty()){
			return Integer.MIN_VALUE;
		}

		return heap.get(1);
	}

	public int removeMax(){
		if(heap.isEmpty()){
			return Integer.MIN_VALUE;
		}

		int min=heap.get(1);
		int lastIndex=heap.size()-1;
		heap.set(1,heap.get(lastIndex));
		heap.remove(lastIndex);

		int parentIndex=1;
		int leftChildIndex=2*parentIndex;
		int rightChildIndex=leftChildIndex+1;

		while(parentIndex<=heap.size()-1){
			int maxIndex=parentIndex;
			int maxValue=heap.get(maxIndex);

			if(leftChildIndex<=heap.size()-1){
				if(heap.get(leftChildIndex)>heap.get(maxIndex)){
					maxIndex=leftChildIndex;
					maxValue=heap.get(maxIndex);
				}
			}

			if(rightChildIndex<=heap.size()-1){
				if(heap.get(rightChildIndex)>heap.get(maxIndex)){
					maxIndex=rightChildIndex;
					maxValue=heap.get(maxIndex);
				}
			}

			if(maxIndex==parentIndex)
				break;
			else{
				int temp=maxValue;
				heap.set(maxIndex,heap.get(parentIndex));
				heap.set(parentIndex,temp);
			}

			parentIndex=maxIndex;
			leftChildIndex=2*parentIndex;
			rightChildIndex=leftChildIndex+1;
		}
		return min;
	}

	public static void main(String[] args) {
		MaxHeap mh = new MaxHeap();
		mh.insert(13);
		mh.insert(45);

		System.out.println(mh.getMax());
	}
}
