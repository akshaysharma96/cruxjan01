package StackAndQueues;

import Lecture15Queue.QueueEmptyException;

public class Queue<T> {
	
	private Node<T> front;
	private Node<T> rear;
	private int size;
	
	public void enqueue(T data){
		if(front==null){
			Node<T> newNode=new Node<T>(data);
			front=newNode;
			rear=newNode;
			size=1;
			return;
		}
		Node<T> newNode= new Node<T>(data);
		rear.next=newNode;
		rear=newNode;
		size++;
	}
	
	public T dequeue() throws QueueEmptyException{
		if(front==null){
			throw new QueueEmptyException();
		}
		T value=front.data;
		front=front.next;
		size--;
		return value;
	}
	
	public boolean isEmpty(){
		return front==null;
	}
	
	public int size(){
		return size;
	}
	
	public T front() throws QueueEmptyException {
		if(front==null)
			throw new QueueEmptyException();
		
		return front.data;
	}
	
	public void reverseQueue() throws QueueEmptyException{
		if(size==1){
			System.out.println(front());
			return;
		}
		T data=dequeue();
		reverseQueue();
		System.out.println(data);
		
	}
}
