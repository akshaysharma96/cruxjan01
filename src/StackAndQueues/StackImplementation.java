package StackAndQueues;

public class StackImplementation {
	public static void main(String[]args){
		Stack<Integer> st= new Stack<Integer>();
		for(int i=0;i<=10;i++){
			System.out.println(i);
			st.push(i);
		}
		for(int i=0;i<=11;i++){
			try {
				System.out.println(st.pop());
				///System.out.println("Size: "+st.size());
			} catch (StackEmptyException e) {
				// TODO Auto-generated catch block
				System.out.println("Stack is empty");
			}
		}
	}
}
