package StackAndQueues;

public class CheckBalancedString {
	private Stack<Character> s1;

	public CheckBalancedString(){
		s1=new Stack<Character>();
	}

	public boolean checkString(String str) throws StackEmptyException{
		for(int i=0;i<str.length();i++){
			if(str.charAt(i)=='{'||str.charAt(i)=='('||str.charAt(i)=='['){
				s1.push(str.charAt(i));
			}
		}
		boolean isBalanced=true;
		for(int i=0;i<str.length();i++){
			if(str.charAt(i)=='}'){
				char ch=s1.pop();
				if(ch!='{'){
					isBalanced=false;
					break;
				}
			}
			if(str.charAt(i)==']'){
				char ch=s1.pop();
				if(ch!='['){
					isBalanced=false;
					break;
				}
			}

			if(str.charAt(i)==')'){
				char ch=s1.pop();
				if(ch!='('){
					isBalanced=false;
					break;
				}
			}
		}

		return isBalanced;
	}
	
	public static void main(String[]args){
		String str="{[()]}";
		CheckBalancedString obj=new CheckBalancedString();
		try {
			System.out.println(obj.checkString(str));
		} catch (StackEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
