package Lecture15Queue;

public class StackUsingQueue<T> {
	Queue<T> q1;
	Queue<T> q2;

	public StackUsingQueue() {
		q1=new Queue<T>();
		q2=new Queue<T>();
	}

	public void push(T data){
		q1.enqueue(data);
	}

	public T pop() throws QueueEmptyException{
		T data;
		
		while(q1.size()!=1){
			data=q1.dequeque();
			q2.enqueue(data);
		}
		data=q1.dequeque();
		Queue<T> temp=q1;
		q1=q2;
		q2=temp;
		return data;
	}
	
	
}
