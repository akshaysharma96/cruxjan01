package Lecture17;

import java.nio.BufferUnderflowException;
import java.util.Scanner;

import org.omg.PortableInterceptor.INACTIVE;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class BTUse {
	public static BinaryTree<Integer> levelWiseInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}

		return root;
	}

	public static void printBinaryTree(BinaryTree<Integer> root){
		if(root==null)
			return;
		String str=root.data+":";
		if(root.left!=null)
			str=str+root.left.data+",";
		if(root.right!=null)
			str=str+root.right.data+",";
		System.out.print(str);
		System.out.println();
		printBinaryTree(root.left);
		printBinaryTree(root.right);
	}

	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}

	public static int getHeight(BinaryTree<Integer> root){
		if(root==null)
			return 0;
		int height=0;
		int max=height;
		max=getHeight(root.left);
		if(max>height)
			height=max;
		max=getHeight(root.right);
		if(max>height)
			height=max;

		return height+1;
	}

	public static int getDiameter(BinaryTree<Integer> root){
		if(root==null)
			return 0;

		int leftDia=getDiameter(root.left);
		int rightDia=getDiameter(root.right);
		int total=getHeight(root.left)+getHeight(root.right)+1;
		return Math.max(leftDia, Math.max(rightDia, total));
	}

	public static int getH(BinaryTree<Integer> root){
		if(root==null){
			return 0;
		}
		int h1=getH(root.left);
		int h2=getH(root.right);
		return Math.max(h1, h2)+1;
	}
	
	public static BinaryTree<Integer> getNode(BinaryTree<Integer> root, int value){
		if(root==null)
			return null;
		if(root.data==value)
			return root;
		else{
			BinaryTree<Integer> node=getNode(root.left, value);
			if(node!=null)
				return node;
		}
		BinaryTree<Integer> node=getNode(root.right, value);
		return node;
	}
	
	public static void mirrorBinaryTree(BinaryTree<Integer> root){
		if(root==null)
			return;
		
		BinaryTree<Integer> temp=root.left;
		root.left=root.right;
		root.right=temp;
		mirrorBinaryTree(root.left);
		mirrorBinaryTree(root.right);
		
	}
	
	public static void preOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		System.out.print(root.data+" ");
		preOrder(root.left);
		preOrder(root.right);
	}
	
	public static void postOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		postOrder(root.left);
		postOrder(root.right);
		System.out.print(root.data+" ");
	}
	
	public static void inOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		inOrder(root.left);
		System.out.print(root.data+" ");
		inOrder(root.right);
	}
	
//	public static BinaryTree<Integer> buildTree(int[] preOrder,int[]inOrder, int preStart, int preEnd, int inStart, int inEnd){
////		if(preStart>=preEnd||inStart>=inEnd)
////			return null;
////		
////		int nodevalue=preOrder[preStart];
////		int index=-1;
////		for(int i=0;i<inOrder.length;i++){
////			if(inOrder[i]==nodevalue){
////				index=i;
////			}
////		}
////		int beforeCount=index-inStart;
////		int afterCount=inEnd-index;
////		BinaryTree<Integer> root=new BinaryTree<Integer>(nodevalue);
////		root.left=buildTree(inOrder,preOrder,preStart+1,preStart+beforeCount-1+1,inStart,index-1);
////		root.right=buildTree(inOrder,preOrder,preStart+beforeCount+1,preStart+beforeCount-1+afterCount+1,index+1,inEnd);
////		
////		return root;
////	}
	public static void main(String[]args) throws QueueEmptyException{
		// 8 10 3 1 6 14 -1 -1 -1 4 7 13 -1 -1 -1 -1 -1 -1 -1
		//BinaryTree<Integer> root=levelWiseInput();
		//printBinaryTree(root);
		//levelWisePrint(root);
		//System.out.println("\n"+getHeight(root));
		//System.out.println("\n"+getH(root));
//		try{
//			System.out.println(getNode(root, 16).data);
//		}catch(NullPointerException e){
//			System.out.println("Node not found");
//		}
		//mirrorBinaryTree(root);
		//levelWisePrint(root);
		//inOrder(root);
		int[] preOrder={1,2,4,5,7,3,6};
		int[] inOrder={4,2,5,7,1,6,3};
//		BinaryTree<Integer> generatedBinaryTree=buildTree(preOrder, inOrder,0,preOrder.length-1,0, inOrder.length-1);
//		levelWisePrint(generatedBinaryTree);
	}
}
