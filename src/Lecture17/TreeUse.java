package Lecture17;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;


public class TreeUse {
	public static TreeNode<Integer> leveWiseINput() throws QueueEmptyException{
		Queue<TreeNode<Integer>> q= new Queue<TreeNode<Integer>>();
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the value of root node: ");
		int data=sc.nextInt();
		TreeNode<Integer> root = new TreeNode<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			TreeNode<Integer> dequeued = q.dequeue();
			System.out.print("Enter the number of children of "+dequeued.data+": ");
			int root_number=sc.nextInt();
			for(int i=0;i<root_number;i++){
				System.out.print("Enter the "+i+"th child of "+dequeued.data+": ");
				int data_value=sc.nextInt();
				TreeNode<Integer> node=new TreeNode<Integer>(data_value);
				q.enqueue(node);
				dequeued.children.add(node);
			}
		}
		sc.close();
		return root;
	}
	
	public static int returnSum(TreeNode<Integer> root){
		int sum=root.data;
		for(TreeNode<Integer> child:root.children)
			sum=sum+child.data;
		
		return sum;
	}
	
	public static TreeNode<Integer> getMaxNode(TreeNode<Integer> root){
		int sum=returnSum(root);
		
		TreeNode<Integer> node=root;
		for(TreeNode<Integer> child :root.children){
			int ans=returnSum(child);
			if(ans>sum){
				sum=ans;
				node=child;
			}
		}
		
		return node;
	}
	
	public static void main(String[]args) throws QueueEmptyException{
		TreeNode<Integer> root=leveWiseINput();
		TreeNode<Integer> maxNode=getMaxNode(root);
		System.out.println(maxNode.data);
	}
}
