package Lecture9;

public class CalculatePolynomial {

	/**
	 * Calculate power of polynomial in O(n) time
	 */
	public static int getPolynomial(int n, int[] arr){
		int value=1;
		int sum=arr[0]*value;
		if(arr.length>=1){
			for(int i=1;i<arr.length;i++){
				value=value*n;
				sum=sum+arr[i]*value;
			}
		}
		
		return sum;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int arr[] ={2,1,3,4};
		System.out.println(getPolynomial(6, arr));
	}

}
