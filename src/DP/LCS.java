package DP;

public class LCS {
	public static int LCSBruteForce(String s,String t){
		if(s.length()==0||t.length()==0){
			return 0;
		}
		if(s.charAt(0)==t.charAt(0)){
			return 1+LCSBruteForce(s.substring(1), t.substring(1));
		}
		else{
			int option1=LCSBruteForce(s.substring(1), t);
			int option2=LCSBruteForce(s, t.substring(1));
			return Math.max(option1, option2);
		}
	}

	public static int LCSDP(String s, String t){
		int[][] arr=new int[s.length()+1][t.length()+1];
		for(int i=0;i<=s.length();i++){
			for(int j=0;j<=t.length();j++){
				arr[i][j]=-1;
			}
		}

		return LCSDP(s,  t,arr);
	}
	private static int LCSDP(String s, String t, int[][] arr) {
		int m=s.length();
		int n=t.length();
		if(m==0||n==0){
			arr[m][n]=0;
			return arr[m][n];
		}
		if(arr[m][n]!=-1){
			return arr[m][n];
		}
		if(s.charAt(0)==t.charAt(0)){
			return 1+LCSDP(s.substring(1), t.substring(1), arr);
		}
		else{
			int option1=LCSDP(s.substring(1), t, arr);
			int option2=LCSDP(s, t.substring(1), arr);
			arr[m][n]=Math.max(option1, option2);
			return arr[m][n];
		}
	}
	
	public static int LCSIteractive(String s, String t){
		int m=s.length()+1;
		int n=t.length()+1;
		int l1=s.length();
		int l2=t.length();
		int[][] arr=new int[m][n];
		for(int i=0;i<m;i++){
			arr[i][0]=0;
		}
		for(int i=0;i<n;i++){
			arr[0][i]=0;
		}
		for(int i=1;i<m;i++){
			for(int j=1;j<n;j++){
				if(s.charAt(l1-i)==t.charAt(l2-j)){
					arr[i][j]=1+arr[i-1][j-1];
				}
				else{
					arr[i][j]=Math.max(arr[i][j-1], arr[i-1][j]);
				}
			}
		}
		
		return arr[m-1][n-1];
	}

	public static void main(String[] args) {
		System.out.println(LCSIteractive("SDFJSDKLFJKLDSJKJDKLJKLJSDKLFJLKDSJKLAJSLKASJDLKSD", "GXTXDKSDJHKJASHDJKASHKDJHASJKDHSKJDHSKJDHSJKHJKDSHFJKDSHAYB"));
//		System.out.println(LCSBruteForce("oiusrljsdlkjflsdkS", "kdsjslkajdlajslkj"));

	}
}
