package DP;

public class Fibonacci {

	public static int fibonacciBruteForce(int n){
		if(n==1){
			return 1;
		}
		if(n==0){
			return 0;
		}
		
		else{
			return fibonacciBruteForce(n-1)+fibonacciBruteForce(n-2);
		}
	}
	
	public static int fibIterative(int n){
		int arr[] = new int[n+1];
		arr[0]=0;
		arr[1]=1;
		for(int i=2;i<n+1;i++){
			arr[i]=arr[i-1]+arr[i-2];
		}
		
		return arr[n];
	}
	
	public static int fibonacciDP(int n){
		int arr[] = new int[n+1];
		for(int i=0;i<n+1;i++){
			arr[i]=-1;
		}
		return fibonacciDP(n,arr);
	
	}
	
	private static int fibonacciDP(int n, int[] arr) {
		if(n==0){
			arr[n]=0;
			return arr[n];
		}
		
		if(n==1){
			arr[n]=1;
			return arr[n];
		}
		
		if(arr[n]!=-1){
			return arr[n];
		}
		else{
			arr[n]=fibonacciDP(n-1, arr)+fibonacciDP(n-2, arr);
			return arr[n];
		}
	}

	public static void main(String[] args) {
		System.out.println(fibIterative(700));
		System.out.println(fibonacciDP(70));
	}
}
