/*
 Suppose you have some string SS having length NN that is indexed from 00 to N−1N−1. You also have some string RR that is the reverse of string SS. SS is funny if the condition | S[j]−S[j−1] |=| R[j]−R[j−1] || S[j]−S[j−1] |=| R[j]−R[j−1] | is true for every jj from 11 to N−1N−1.

Note: For some string SS, S[j]S[j] denotes the ASCII value of the jth zero-indexed character in SS. The absolute value of some integer xx is written as | x || x |.

Input Format

The first line contains an integer, TT (the number of test cases). 
The TT subsequent lines each contain a string, where the ith line is string SiSi.

Constraints 
1≤T≤101≤T≤10 
0≤i≤T−10≤i≤T−1 
2≤length of Si≤100002≤length of Si≤10000

Output Format

For each SiSi, print Funny or Not Funny on a new line.

*/

package HackerRank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


public class FunnyStrings {

	public static String getReverse(String str){
		String str2="";
		for(int i=str.length()-1;i>=0;i--){
			str2=str2+str.charAt(i);
		}
		return str2;
	}

	public static void isFunny(String str){
		if(str.length()==0||str.length()==1){
			System.out.print("Not Funny");
			return;
		}
		if(str.length()==2){
			System.out.println("Funny");
			return;
		}
		else{
			String reverse=getReverse(str);
			System.out.println(reverse);
			for(int i=1;i<str.length();){
				if(Math.abs(str.charAt(i)-str.charAt(i-1))==Math.abs(reverse.charAt(i)-reverse.charAt(i-1))){
					i++;
				}
				else{
					System.out.println("Not Funny");
					return;
				}
			}
			
			System.out.println("Funny");
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//		int count=sc.nextInt();
		//		String str;
		//		for(int i=0;i<count;i++){
		//			str=sc.nextLine();
		//
		//		}
		isFunny("acxz");
	}
}
