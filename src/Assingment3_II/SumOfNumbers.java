package Assingment3_II;

public class SumOfNumbers {


	public static int[] mergeSort(int []arr, int startIndex,int endIndex){
		if(startIndex>=endIndex){
			return arr;
		}
		int[] half1=new int[arr.length/2];
		int[] half2=new int[arr.length-half1.length];
		int i;
		for(i=0;i<half1.length;i++){
			half1[i]=arr[i];
		}
		for(int k=0;k<half2.length;k++,i++){
			half2[k]=arr[i];
		}

		half1=mergeSort(half1, 0, half1.length-1);
		half2=mergeSort(half2, 0, half2.length-1);

		return merge(half1,half2);

	}
	
	private static int[] merge(int[] half1,int[] half2) {
		int i=0,k=0,j=0;
		int result[] = new int[half1.length+half2.length];
 		while(i<half1.length&&j<half2.length){
 			if(half1[i]<half2[j]){
 				result[k++]=half1[i++];
 			}
 			else{
 				result[k++]=half2[j++];
 			}
 		}
 		
 		while(i<half1.length){
 			result[k++]=half1[i++];
 		}
 		
 		while(j<half2.length){
 			result[k++]=half2[j++];
 		}
 		
 		return result;
	}
	
	public static void main(String []args){
		int[] arr={5,2,8,1,7,45,78};
		findSum(arr,12);
//		arr=mergeSort(arr,0,6);
//		for(int i:arr){
//			System.out.println(i);
//		}
	}

	private static void findSum(int[] arr, int i) {
		arr=mergeSort(arr,0,arr.length-1);
		int k=0;
		int j=arr.length-1;
		while(k<j){
			if(arr[k]+arr[j]==i){
				System.out.println("sum found");
				return;
			}
			if(arr[k]+arr[j]>i){
				j--;
			}
			if(arr[k]+arr[j]<i){
				k++;
			}
		}
		
		System.out.println("Not found");
	}
}
