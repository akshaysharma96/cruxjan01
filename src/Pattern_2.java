import java.util.Scanner;


public class Pattern_2 {

	/**
	 * @param args
	 */
	
	/*
	 * Display pattern like: 
	 *    1
	 *   232
	 *  34543
	 * 4567654
	 * ........
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		N=sc.nextInt();
		int rows=1;
		int value;
		while(rows<=N){
			int spaces=1;
			while(spaces<=N-rows){
				System.out.print(" ");
				spaces++;
			}
			value=rows;
			int col=1;
			while(col<=rows){
				System.out.print(value+"");
				value++;
				col++;
			}
			 col=1;
			 value=value-2;
			 while(col<rows){
				 System.out.print(value+"");
				 value--;
				 col++;
			 }
			 rows++;
			System.out.println();
		}
		
	}

}
