package Recursion2;

public class Q10 {

	/**
	 * Print all the combinations of hops a child can make if it can do 1,2 or 3 steps at a time.
	 */

	public static void getSteps(int n,String output){
		if(n==0){
			System.out.print(output);
			System.out.println();
			return;
		}
		if(n<0){
			System.out.print("");
		}
		else{
			for(int i=1;i<=3;i++){
				getSteps(n-i,output+" "+i);
			}
		}

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		getSteps(4, "");
	}

}
