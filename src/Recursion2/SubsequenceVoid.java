package Recursion2;

public class SubsequenceVoid {

	/**
	 * @param args
	 */
	public static void subsequence(String str, String output){
		if(str.length()==0){
			System.out.print(output+" ");
			return;
		}
		subsequence(str.substring(1), output);
		subsequence(str.substring(1), output+str.charAt(0));
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		subsequence("abc", "");
	}

}
