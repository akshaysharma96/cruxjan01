package Recursion2;

public class Subsequences {

	/**
	 * Print all subsequences of a string
	 */
	public static String[] subsequnece(String str){
		if(str.length()==0){
			return new String[] {""};
		}
		
		String[] out =subsequnece(str.substring(1));
		String result[] = new String[2*out.length];
		int i,j,k;
		for(i=0;i<out.length;i++){
			result[i]=out[i];
		}
		for(j=i,k=0;j<result.length;j++,k++){
			result[j]=str.charAt(0)+out[k];
		}
		
		return result;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] s =subsequnece("ab");
		for(int i=0;i<s.length;i++){
			System.out.print(s[i]+" ");
		}
	}

}
