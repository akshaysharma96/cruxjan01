package Recursion2;

public class MergeSort {

	/**
	 * @param args
	 */
	
	public static int[] merge(int a[],int b[]){
		int[] result= new int[a.length+b.length];
		int i=0;
		int j=0;
		int k=0;
		while(i<a.length&&j<b.length){
			if(a[i]<b[j]){
				result[k++]=a[i++];
			}
			else{
				result[k++]=b[j++];
			}
		}
		while(i<a.length){
			result[k++]=a[i++];
		}
		while(j<b.length){
			result[k++]=b[j++];
		}
		
		return result;
	}
	
	public static int[] mergesort(int a[]){
		if(a.length<=1){
			return a;
		}
		int[] half1= new int[a.length/2];
		int[] half2=new int[a.length-half1.length];
		int i=0;
		int k=0;
		for(i=0;i<half1.length;i++){
			half1[i]=a[i];
		}
		for(k=0;k<half2.length;i++,k++){
			half2[k]=a[i];
		}
		
		half1=mergesort(half1);
		half2=mergesort(half2);
		int[] output=merge(half1,half2);
		return output ;
	}
	public static void main(String[] args) {
		int[] a={5,7,9,13,67};
		int[] b={1,2,10,12,69};
		int[] result=merge(a, b);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
		System.out.println();
		int[] r={5,34,12,65,1,45,0};
		r=mergesort(r);
		for(int i=0;i<r.length;i++){
			System.out.print(r[i]+" ");
		}
		
	}

}
