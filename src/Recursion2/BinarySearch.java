package Recursion2;

public class BinarySearch {

	/**
	 * Binary Search using recursion
	 */
	public static int  binarySearch(int[] a,int start,int end, int element){
		int mid = (start+end)/2;
		
		if(start==end){
			if(a[start]==element)
				return start;
			else
				return -1;
		}
		
		if(a[mid]==element){
			return mid;
		}
		if(element<a[mid]){
			return binarySearch(a, start,mid-1, element);
		}
		else{
			return binarySearch(a, mid+1, end, element);
		}
	}
	public static void main(String[] args) {
		int a[]={1,2,3,4,5,6,7,8};
		int index=binarySearch(a, 0,7,9);
		System.out.println(index);
	}

}
