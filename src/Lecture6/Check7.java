package Lecture6;

public class Check7 {

	/**
	 * Check if 7 is contained in an array
	 */
	public static boolean isSorted(int []array){
		if(array.length<=1){
			return true;
		}

		else if(array[0]>array[1]){
			return false;
		}

		int[] newArr = new int[array.length-1];
		for(int i=1;i<array.length;i++){
			newArr[i-1]=array[i];
		}
		return isSorted(newArr);

	}
	public static boolean check7(int array[],int index){
		if(index>=array.length){
			return false;
		}
		else if(array[index]==7){
			return true;
		}
		else{
			return check7(array,index+1);
		}
	}

	public static int indexFirst7(int array[],int index){
		if(index>=array.length){
			return -1;
		}
		else if(array[index]==7){
			return index;
		}
		else{
			return indexFirst7(array,index+1);
		}
	}

	public static int indexLast7(int array[],int index){
		if(index<0){
			return -1;
		}
		else if(array[index]==7){
			return index;
		}
		else{
			return indexLast7(array,index-1);
		}
	}

	/*public static int[] indicesArray(int []array,int[] indices,int index,int i){
		if(index>=array.length){
			return new int[i];
		}
		if(array[index]==7){
			i++;
			index++;
			return indicesArray(array, indices, index, i);
		}
		else{
			index++;
			return indicesArray(array, indices, index, i);
		}
	}
	*/
	public static int lastIndex7(int[] array,int index,int count){
		if(index>=array.length){
			return count;
		}
		
		if(array[index]==7){
			count=index;
			index++;
			return lastIndex7(array, index, count);
		}
		
		else{
			index++;
			return lastIndex7(array, index, count);
		}
		
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={1,2,3,7};
		int[] array2={1,2,3,8,7,9,67,7};
		System.out.println(check7(array,0));
		System.out.println(indexFirst7(array2,0));
		int[] indices=new int[8];
		for(int i=0;i<indices.length;i++)
			indices[i]=-1;
		
		System.out.println(indexLast7(array2, array2.length-1));
		//int[] newarr=indicesArray(array2, indices, 0, 0);
		/*for(int i=0;i<newarr.length;i++){
			System.out.print(newarr[i]+" ");
		}*/
		System.out.println(isSorted(array2));
		
		System.out.println(lastIndex7(array2, 0, -1));
	}

}
