package Lecture6;

public class RecursionPower {

	/**
	 * X^n using recursion
	 */
	public static int getPower(int x, int n){
		if(n==1){
			return x;
		}
		else{
			return x*getPower(x,n-1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getPower(4, 3));
	}

}
