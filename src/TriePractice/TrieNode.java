package TriePractice;

import java.util.HashMap;

public class TrieNode {
	
	char name;
	HashMap<Character, TrieNode> children;
	boolean isTerminal;
	
	
	public TrieNode(char name) {
		// TODO Auto-generated constructor stub
		this.name=name;
		children=new HashMap<Character,TrieNode>();
		isTerminal=false;
	}
}
