package TriePractice;

public class Trie {
	
	TrieNode root;
	
	public Trie() {
		// TODO Auto-generated constructor stub
		root=new TrieNode('\0');
	}
	
	public void addWord(String str){
		addWord(root,str);
	}
	
	public void addWord(TrieNode root,String str){
		
		if(str.length()==0){
			root.isTerminal=true;
			return;
		}
		
		char first=str.charAt(0);
		if(root.children.containsKey(first)){
			TrieNode child=root.children.get(first);
			addWord(child, str.substring(1));
		}
		else{
			TrieNode child=new TrieNode(first);
			root.children.put(first, child);
			addWord(child, str.substring(1));
		}
	}
	
	public void removeWord(String str){
		removeWord(root,str);
	}

	private void removeWord(TrieNode root, String str) {
		// TODO Auto-generated method stub
		if(str.length()==0){
			root.isTerminal=false;
			return;
		}
		char first=str.charAt(0);
		if(root.children.containsKey(first)){
			TrieNode child=root.children.get(first);
			removeWord(child, str.substring(1));
			if(!child.isTerminal&&child.children.size()==0){
				root.children.remove(child);
			}
		}else{
			System.out.println("String not found");
		}
	}
	
	public boolean findWord(String str){
		return findWord(root,str);
	}

	private boolean findWord(TrieNode root, String str) {
		// TODO Auto-generated method stub
		if(str.length()==0){
			return root.isTerminal;
		}
		char first=str.charAt(0);
		if(root.children.containsKey(first)){
			TrieNode child=root.children.get(first);
			return findWord(child, str.substring(1));
		}else{
			return false;
		}
	}
	
	public static void main(String[] args) {
		Trie t = new Trie();
		t.addWord("djldsjdjasdj");
		System.out.println(t.findWord("djldsjdjasdj"));
	}
}
