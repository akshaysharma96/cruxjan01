import java.util.Scanner;


public class TemperatureConversion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Convert Fahrenheit to Celsius
		// C=(5/9)*F-32
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter starting Fahrenheit value: ");
		int startingFahrenheitValue=sc.nextInt();
		System.out.print("Enter ending Fahrenheit value: ");
		int endingFahrenheitValue=sc.nextInt();
		
		int fahrenheit=startingFahrenheitValue;
		while(fahrenheit<=endingFahrenheitValue){
			int celsius= (int) ((5.0/9.0)*(fahrenheit-32));
			//Since ((5.0/9/0)*(fahrenheit) gives a double value to get int value we cast it )
			System.out.println(fahrenheit+"\t"+celsius);
			fahrenheit=fahrenheit+20;
			}
		System.out.println("--------");
		//Prints 195 = 96(ASCII value of 'a') +97(ASCII value of 'b')
		System.out.println('a'+'b');
		char z='a'+'b';
		//prints character with ASCII value = ASCII value of 'a'+ ASCII value of 'b'
		System.out.print(z);
	}

}
